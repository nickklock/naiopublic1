package commands.sro;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.EMBEDS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

public class Balance implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        if (args.length < 13) {
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            return;
        }
        String ratioStr = args[0];
        List<String> ratio = Arrays.asList(ratioStr.split(":"));

        String balanceInt = ratio.get(0);
        String balanceStr = ratio.get(1);
        double totalStr = (11*6)+(11*3);
        double totalInt = (11*6)+(11*3);
        double extraHp = (600*3) + (300 * 6);
        double extraMp = (600*3) + (300 * 6);
        double statPointsPerLevel = 3;
        double level =  Double.parseDouble(args[1]);
        double baseStr = 20;
        double baseInt = 20;

        double extraHMPPercent = Double.parseDouble(args[2]);

        double extraPhyMagDmg = 5;

        double phyAttackPowerMin = Double.parseDouble(args[3]);
        double phyAttackPowerMax = Double.parseDouble(args[4]);
        double jobAttackPowerMin = Double.parseDouble(args[5]);
        double jobAttackPowerMax = Double.parseDouble(args[6]);

        double totalPhyAttackPowerMin = phyAttackPowerMin + jobAttackPowerMin;
        double totalPhyAttackPowerMax = phyAttackPowerMax + jobAttackPowerMax;

        double phyReinMin = Double.parseDouble(args[7]);
        double phyReinMax = Double.parseDouble(args[8]);

        double magAttackPowerMin = Double.parseDouble(args[9]);
        double magAttackPowerMax = Double.parseDouble(args[10]);
        double jobMagAttackPowerMin = Double.parseDouble(args[11]);
        double jobMagAttackPowerMax = Double.parseDouble(args[12]);

        double totalMagAttackPowerMin = magAttackPowerMin + jobMagAttackPowerMin;
        double totalMagAttackPowerMax = magAttackPowerMax + jobMagAttackPowerMax;

        double magReinMin = Double.parseDouble(args[13]);
        double magReinMax = Double.parseDouble(args[14]);

        double balancePointsTotal = Double.parseDouble(balanceStr)+Double.parseDouble(balanceInt);
        double statPointsTotal = (level -1) * statPointsPerLevel;

        double balanceTotalStr = statPointsTotal*(Double.parseDouble(balanceStr)*1.0/balancePointsTotal);
        double balanceTotalInt = statPointsTotal*(Double.parseDouble(balanceInt)*1.0/balancePointsTotal);

        double STR = baseStr + (level-1)+ balanceTotalStr + totalStr;
        double INT = baseInt + (level-1)+ balanceTotalInt + totalInt;

        double HP = Math.round(Math.pow(1.02,(level -1)) * STR * 10) + extraHp;
        double MP = Math.round(Math.pow(1.02,(level -1)) * INT * 10) + extraMp;
        HP = HP + (extraHMPPercent * HP /100);
        MP = MP + (extraHMPPercent * MP /100);

        double M = 28 +level*4; //388
        double we = (100 *2)/3;
        double phyBalance = 0;
        double mstr = M - STR;
        phyBalance = we*mstr;
        phyBalance = phyBalance / M;

        phyBalance = 100 - phyBalance;

        double magBalance = 100 *INT;
        magBalance = magBalance / M ;

        if (phyBalance > 120){
            phyBalance = 120;
        }

        if (magBalance > 120){
            magBalance = 120;
        }

        double phyAttackMin = (totalPhyAttackPowerMin+(STR*phyReinMin/100)) * (1+extraPhyMagDmg/100);
        double phyAttackMax = (totalPhyAttackPowerMax+(STR*phyReinMax/100)) * (1+extraPhyMagDmg/100);
        double magAttackMin = (totalMagAttackPowerMin+(INT*magReinMin/100)) * (1+extraPhyMagDmg/100);
        double magAttackMax = (totalMagAttackPowerMax+(INT*magReinMax/100)) * (1+extraPhyMagDmg/100);

        double critDmgMin = phyAttackMin*(1+phyBalance/100)+magAttackMin;
        double critDmgMax = phyAttackMax*(1+phyBalance/100)+magAttackMax;

        EmbedBuilder eb = new EmbedBuilder().setColor(Color.decode("#3c40c6")).setTitle("Calculation for: "+args[0])
                .setDescription("Full Blue is assumed in this calculation.")
                .addField("HP",String.valueOf(HP),true)
                .addField("MP",String.valueOf(MP),true)
                .addBlankField(true)
                .addField("STR",String.valueOf(STR),true)
                .addField("INT",String.valueOf(INT),true)
                .addField("Distributed stat points",String.valueOf(statPointsTotal),true)

                .addField("Physical balance", String.valueOf(Math.round(phyBalance))+ "%",true)
                .addField("Magical balance", String.valueOf(Math.round(magBalance))+ "%",true)
                .addBlankField(true)
                .addField("Phy attack power",Math.round(phyAttackMin) + " to "+Math.round(phyAttackMax), true)
                .addField("Mag attack power",Math.round(magAttackMin) + " to "+Math.round(magAttackMax), true)
                .addField("Total attack power",Math.round(phyAttackMin + magAttackMin) + " to " +
                        ""+Math.round(phyAttackMax+magAttackMax),true)
                .addField("Crit damage",Math.round(critDmgMin)+" to "+Math.round(critDmgMax),true)
                ;

        event.getTextChannel().sendMessage(eb.build()).queue();


    }

    @Override
    public String help(){
        return  "[int:str] [level] [devil] [wepPhyMin] [wepPhyMax] [jobWepPhyMin] [jobWepPhyMax] [phyReinforceMin] [phyReinforceMax] " +
                "[wepMagMin] [wepMagMax] [jobWepMagMin] [jobWepMagMax] [magReinforceMin] [magReinforceMax]";
    }

    @Override
    public String description(){
        return "Calculates values for different builds";
    }
}
