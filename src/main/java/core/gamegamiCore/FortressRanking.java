package core.gamegamiCore;

import core.models.FortressRankingModel;
import net.dv8tion.jda.api.EmbedBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class FortressRanking {

    public static EmbedBuilder getEmbed(int part, String id){
        ArrayList<FortressRankingModel> rankingsList = getGRanking(id);

        EmbedBuilder embedBuilder1 = new EmbedBuilder();
        embedBuilder1.setTitle("FW Guild Ranking (1/2)");
        embedBuilder1.setColor(Color.decode("#3c40c6"));
        switch (part){
            case 1:
                for (int i = 0; i< 24; i++){
                    FortressRankingModel guildRank = rankingsList.get(i);
                    embedBuilder1.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "K/D: "+guildRank.getKd()+"\n"+guildRank.getKdq(),true);
                }
                return embedBuilder1;

            case 2:
                EmbedBuilder embedBuilder2 = new EmbedBuilder(embedBuilder1).setTitle("FW Guild Ranking (2/2)");
                for (int i = 25; i< rankingsList.size(); i++){
                    FortressRankingModel guildRank = rankingsList.get(i);
                    embedBuilder2.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "K/D: "+guildRank.getKd()+"\n"+guildRank.getKdq(),true);
                }
                return embedBuilder2;
        }
        return null;
    }

    public static ArrayList<FortressRankingModel> getGRanking(String id){
        Document doc;
        try {
            doc = Jsoup.connect("https://silkroad.gamegami.com/ranking_fortress_guild.php?id="+id)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            Element guildTable = doc.select("tbody").get(0);
            Elements rows = guildTable.select("tr");
            ArrayList<FortressRankingModel> ranks = new ArrayList<>();

            for (int i = 0; i < rows.size(); i++){
                Element row = rows.get(i);
                Elements cols = row.select("td");
                FortressRankingModel gRank = new FortressRankingModel(null,null,null);
                for (int a = 0; a < cols.size() ; a++){
                    String colText = cols.get(a).text();
                    switch (a){
                        case 1:
                            gRank.setRank(colText);
                            break;
                        case 2:
                            gRank.setName(colText);
                            break;
                        case 3:
                            List<String> kd = Arrays.asList(colText.split("/"));
                            double kdql = Double.parseDouble(kd.get(0).trim()) / Double.parseDouble(kd.get(1).trim());
                            gRank.setKdq(String.valueOf(round(kdql,2)));
                            gRank.setKd(colText);

                            break;
                    }
                }
                if (gRank.getName() != null){
                    ranks.add(gRank);
                }
            }
            return ranks;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
