package core.gamegamiCore;

import core.models.HonorPlayer;
import net.dv8tion.jda.api.EmbedBuilder;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class HonorRanking {
    public static EmbedBuilder getEmbed(int part){
        try {
            ArrayList<HonorPlayer> rankingsList = getRankingsList();

            EmbedBuilder embedBuilder1 = new EmbedBuilder();
            embedBuilder1.setTitle("Honor Ranking (1/2)");
            embedBuilder1.setColor(Color.decode("#3c40c6"));
            switch (part){
                case 1:
                    for (int i = 0; i< 24; i++){
                        HonorPlayer honorPlayer = rankingsList.get(i);
                        embedBuilder1.addField(getEmoji(honorPlayer.getRank())+"-"+honorPlayer.getPlayer(),
                                "Graduates: "+honorPlayer.getGraduates()+
                                        "\nRank: "+honorPlayer.getRank()+
                                        "\nGuild: "+honorPlayer.getGuild(),true);
                    }
                    return embedBuilder1;

                    case 2:
                        EmbedBuilder embedBuilder2 = new EmbedBuilder(embedBuilder1).setTitle("Honor Ranking (2/2)");
                        for (int i = 25; i< rankingsList.size(); i++){
                            HonorPlayer honorPlayer = rankingsList.get(i);
                            embedBuilder2.addField(getEmoji(honorPlayer.getRank())+"-"+honorPlayer.getPlayer(),
                                    "Graduates: "+honorPlayer.getGraduates()+
                                            "\nRank: "+honorPlayer.getRank()+
                                            "\nGuild: "+honorPlayer.getGuild(),true);
                        }
                        return embedBuilder2;
        }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return null;
        }
     return null;
    }


    public static ArrayList<HonorPlayer> getRankingsList() throws IOException, ParseException {
        OkHttpClient client = new OkHttpClient();
        String credentials = Credentials.basic("yanick","Alooooooooo_123!");
        Request request = new Request.Builder()
                .url("https://yanick:Alooooooooo_123!@bscripts.dev:8443/sro/honor")
                .get()
                .addHeader("Authorization",credentials)
                .build();
        Response response = client.newCall(request).execute();
        //System.out.println(response.body().string());
        ArrayList<HonorPlayer> hp = new ArrayList<>();
        JSONParser JSONPARSER = new JSONParser();
        JSONArray jsonArray = (JSONArray) JSONPARSER.parse(response.body().string());
        for (int i = 0; i < jsonArray.size() ; i++){
            JSONObject jo = (JSONObject) jsonArray.get(i);
            hp.add(new HonorPlayer(Integer.parseInt(jo.get("rank").toString()),jo.get("name").toString(),
                    Integer.parseInt(jo.get("graduates").toString()),jo.get("guild").toString()));
        }
        response.close();
        return hp;
    }

    private static String getEmoji(int rank){
        String emoji = "";
        if (rank <= 5){
             emoji = "👑";
        }
        if (rank > 5 && rank <= 15){
             emoji = "\uD83E\uDD47";
        }
        if (rank > 15 && rank <= 30){
            emoji = "🥈";
        }
        if (rank > 30){
            emoji = "🥉";
        }
        return emoji;
    }
}
