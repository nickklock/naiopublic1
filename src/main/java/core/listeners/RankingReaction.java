package core.listeners;

import core.config.ConfigVals;
import core.gamegamiCore.*;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.G_CONFIG;
import util.STATICS;

import javax.annotation.Nonnull;

public class RankingReaction extends ListenerAdapter {
    private static int page = 0;

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());

        if (!event.getMessageId().equals(guildConfig.getGG_RANKING_MSG_ID())) return;
        if (event.getUser().isBot()) return;

        Message rankingMsg = event.getTextChannel().retrieveMessageById(event.getMessageId()).complete();
        String emoji = event.getReactionEmote().getEmoji();

        if (emoji.equals("➡️")){
            if (page < 11){
                page++;
            }else {
                page = 0;
            }
        }else if (emoji.equals("⬅️")){
            if (page == 0){
                page = 11;
            }else {
                page--;
            }
        }
        String id = Servers.getID(event.getGuild().getId());
        System.out.println(id);
        switch (page){
            case 0: // honor1
                rankingMsg.editMessage(HonorRanking.getEmbed(1).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 1: // honor2
                rankingMsg.editMessage(HonorRanking.getEmbed(2).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 2: // guild1
                rankingMsg.editMessage(GuildRanking.getEmbed(1,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 3: // guild2
                rankingMsg.editMessage(GuildRanking.getEmbed(2,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 4: // player1
                rankingMsg.editMessage(PlayerRanking.getEmbed(1,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 5: // player2
                rankingMsg.editMessage(PlayerRanking.getEmbed(2,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 6: // unique1
                rankingMsg.editMessage(UniqueRanking.getEmbed(1,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 7: // unique2
                rankingMsg.editMessage(UniqueRanking.getEmbed(2,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 8: // fw ranking1
                rankingMsg.editMessage(FortressRanking.getEmbed(1,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 9: // fw ranking2
                rankingMsg.editMessage(FortressRanking.getEmbed(2,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 10: // fw player ranking1
                rankingMsg.editMessage(FortressPlayerRanking.getEmbed(1,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;
            case 11: // fw player ranking2
                rankingMsg.editMessage(FortressPlayerRanking.getEmbed(2,id).setFooter("Page "+(page+1)+"/12").build()).queue();
                break;

        }
        rankingMsg.removeReaction(emoji,event.getUser()).queue();
    }
}
