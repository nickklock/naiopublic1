package core.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Bd{
    private  String id;
    private LocalDate bd;
    private int futureAge;

    public int getFutureAge() {
        return futureAge;
    }

    public void setFutureAge(int futureAge) {
        this.futureAge = futureAge;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public LocalDate getBd(){
        return bd;
    }

    public void setBd(LocalDate bd){
        String bdString = bd.format(DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMAN));
        this.bd = bd;
    }
}
