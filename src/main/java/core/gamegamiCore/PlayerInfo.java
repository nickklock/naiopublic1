package core.gamegamiCore;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PlayerInfo {

    public static core.models.PlayerInfo getInfo(String name, String id) throws IOException {
        //https://silkroad.gamegami.com/character.php?shardid=2&char=xDodo

        String URL = "https://silkroad.gamegami.com/character.php?shardid="+Servers.getID(id)+"&char="+name;
        Document doc = Jsoup.connect(URL).get();
        Element img = doc.select("img").get(1);
        String src = img.absUrl("src");

        core.models.PlayerInfo pi = new core.models.PlayerInfo(null,null,null,null,null,
                name,doc.select("[id=chardetail_label_charpoint]").text(),
                doc.select("[id=chardetail_label_charlevel]").text(),doc.select("[id=hpbar]").text()
                , doc.select("[id=mpbar]").text(),doc.select("[class=strintbar]").text());

        Element infoTable = doc.select("tbody").get(0);
        Elements rows = infoTable.select("tr");
        List<String> setTurk = new ArrayList<>();

        for (Element e :
                doc.getAllElements()) {
            if (/*e.text().contains("Spear") && */e.id().contains("gg_equip_item_")) {
                setTurk.add(e.text().split("İ")[0]);
            }
        }
        List<String> setEng = new ArrayList<>();

        for (String s :
                setTurk) {
            if (s.contains("Ay Mührü")){
                String myOutputWithRegEX = Pattern.compile("Ay Mühru").matcher("Ay Mühru").replaceAll("Seal of Moon");
                String[] split = s.split("\\)");
                String done = split[0] + ") *"+myOutputWithRegEX+"*";
                System.out.println(done);
                setEng.add(done);
            }else
            if (s.contains("Güneş Mührü")){
                String myOutputWithRegEX = Pattern.compile("Güneş Mühru").matcher("Güneş Mühru").replaceAll("Seal of Sun");
                String[] split = s.split("\\)");
                String done = split[0] + ") *"+myOutputWithRegEX+"*";
                System.out.println(done);
                setEng.add(done);
            }else
            if (s.contains("Yildiz Mührü")){
                String myOutputWithRegEX = Pattern.compile("Yildiz Mührü").matcher("Yildiz Mührü").replaceAll("Seal of Star");
                String[] split = s.split("\\)");
                String done = split[0] + ") *"+myOutputWithRegEX+"*";
                System.out.println(done);
                setEng.add(done);
            }else if(!s.contains("(F)") || !s.contains("(M)")){
                setEng.add(s);
            }
        }

        StringBuilder sb = new StringBuilder();

        setEng.forEach(s -> {
            sb.append(s).append("\n");
        });

        for (int i = 1 ; i < rows.size(); i++){
            Element row = rows.get(i);
            Elements cols = row.select("td");

            for (int a = 0; a < cols.size(); a++){
                if (a == 2 && i == 1 ){
                    pi.setAlias(cols.get(a).text());
                }
                if (a == 2 && i == 2 ){
                    pi.setGuild(cols.get(a).text());
                }
                if (a == 2 && i == 5 ){
                    pi.setHonor(cols.get(a).text());
                }
                if (a == 2 && i == 6 ){
                    pi.setTitle(cols.get(a).text());
                }
            }
        }
        pi.setUrl(URL);
        pi.setThumb(src);
        pi.setLastestGlobal(getLatestGlobal(doc));
        pi.setLatestUniqueKill(getLatestUnique(doc));
        pi.setSet(sb.toString());
        return pi;


    }

    private static String getLatestGlobal(Document doc){
        Elements latestgloblasElements = doc.select("[class=table_chardetail_globals]");
        Element latestGlobal = latestgloblasElements.select("tbody").get(0);
        Elements global = latestGlobal.select("tr");

        Element globalRow = global.get(0);
        Elements td = globalRow.select("td");
        return td.get(0).text();
    }
    private static String getLatestUnique(Document doc){
        Elements latestgloblasElements = doc.select("[class=table_chardetail_unique]");
        Element latestGlobal = latestgloblasElements.select("tbody").get(0);
        Elements global = latestGlobal.select("tr");

        Element globalRow = global.get(0);
        Elements td = globalRow.select("td");
        return td.get(0).text();
    }
}
