package core.StreamingCore;

import com.github.philippheuer.events4j.simple.SimpleEventHandler;
import com.github.twitch4j.chat.events.TwitchEvent;
import com.github.twitch4j.events.ChannelGoLiveEvent;
import com.github.twitch4j.helix.domain.Game;
import com.github.twitch4j.helix.domain.GameList;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import util.STATICS;

import java.awt.*;
import java.util.Arrays;

public class TwitchStreaming{

    /**
     * Register events of this class with the EventManager/EventHandler
     *
     * @param eventHandler SimpleEventHandler
     */

    public TwitchStreaming(SimpleEventHandler eventHandler){
        eventHandler.onEvent(ChannelGoLiveEvent.class, this::onLive);

    }
    private void onLive(ChannelGoLiveEvent event){

        sendMessage(event);
    }

    private void sendMessage(ChannelGoLiveEvent event){
        STATICS.CONFIGS.forEach((id, config) ->{
            if (config.isSTREAMER_ENABLED()){
                if (config.getSTREAMER_NAMES() != null && config.getSTREAMER_NAMES().contains(event.getChannel().getName())){

                    TextChannel textChannelById = STATICS.JDA.getGuildById(id).getTextChannelById(config.getSTREAM_ANNOUCMENT_CHANNEL());
                    GameList gameList = STATICS.TWITCH.getHelix().getGames(null,Arrays.asList(event.getStream().getGameId()), null).execute();
                    Game game = gameList.getGames().get(0);
                    EmbedBuilder eb = new EmbedBuilder().setColor(Color.decode("#6441a5"))
                            .setTitle(event.getChannel().getName() +" is now live on Twitch")
                            .setDescription
                                    ("["+event.getStream().getTitle()+"]"+"(https://www.twitch.tv/"+event.getChannel().getName()+")\n" +
                                            "\n" +
                                            "Playing "+game.getName())
                            .setImage(event.getStream().getThumbnailUrl(1080,720))
                            .setThumbnail(game.getBoxArtUrl(144,192))
                            ;

                    textChannelById.sendMessage(eb.build()).queue();
                }
            }

        });
    }
}
