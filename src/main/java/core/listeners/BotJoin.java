package core.listeners;

import core.config.GuildConfig;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.G_CONFIG;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class BotJoin extends ListenerAdapter{

    @Override
    public void onGuildJoin(@Nonnull GuildJoinEvent event){
        // create default config file
        createConfig(event);
        createBirthday(event);
        createTriggers(event);
        GuildConfig.readGuildConfigs();

    }

    @Override
    public void onGuildLeave(@Nonnull GuildLeaveEvent event){
        // delete config file
        System.out.println("left");
        deleteGuildFiles(event);
    }

    private static void createConfig(GuildJoinEvent event){
        try {
            Path originalPath =  Paths.get("./default_config.json");
            Path copied = Paths.get("./guild_configs/"+ event.getGuild().getId()+"_config.json");
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createBirthday(GuildJoinEvent event){
        try {
            Path originalPath =  Paths.get("./default_birthdays.json");
            Path copied = Paths.get("./guild_configs/"+ event.getGuild().getId()+"_birthdays.json");
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createTriggers(GuildJoinEvent event){
        try {
            Path originalPath =  Paths.get("./default_triggers.json");
            Path copied = Paths.get("./guild_configs/"+ event.getGuild().getId()+"_triggers.json");
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void deleteGuildFiles(GuildLeaveEvent event){
        Path configGile = Paths.get("./guild_configs/"+ event.getGuild().getId()+"_config.json");
        Path birthdayFile =  Paths.get("./guild_configs/"+ event.getGuild().getId()+"_birthdays.json");
        Path triggersFile =  Paths.get("./guild_configs/"+ event.getGuild().getId()+"_triggers.json");
        try {
            Files.delete(configGile);
            Files.delete(birthdayFile);
            Files.delete(triggersFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
