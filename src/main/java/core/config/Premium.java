package core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import util.STATICS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Premium {

    public static void readPremGuilds(){
        Path p = Paths.get("./premium.json");
        try {
            Prem prems = new ObjectMapper().readValue(new String(Files.readAllBytes(p)),Prem.class);
            STATICS.PREMIUM = prems.getIds();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addPrem(String id){
        Prem prems = new Prem();
        STATICS.PREMIUM.add(id);
        prems.setId(STATICS.PREMIUM);
        Path p = Paths.get("./premium.json");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(p.toFile(),prems);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
