package commands.community;

import com.vdurmont.emoji.EmojiParser;
import commands.Command;
import core.config.ConfigVals;
import core.config.GuildConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import util.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ReactionRole implements Command{
    private static ConfigVals guildConfig;
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if(POWER.noPower(event.getMember())) {
            event.getTextChannel().sendMessage(EMBEDS.nopowerEmbed().build()).queue();
            CMD_REACTION.negative(event);
            return;
        }
        if (args.length<1){
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            return;
        }
        if (guildConfig.getREACTION_ROLE_CHANNEL().isEmpty()){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure the Reaction Role Channel \n" +
                    "use " + guildConfig.getPREFIX() + " admin change reactionrole #channel");

            event.getTextChannel().sendMessage(embedBuilder.build()).queue();
            return;
        }
        if (args[0].equals("setup")) {
            setUp(event);
            return;
        }
        if (!guildConfig.isRR_ROLE_ENABLED()) return;

        String emoteMention= null;
        String emoteName = null;
        Role role= null;
        String roleId = null;
        String roleAsMention= null;
        Message reactionRoleMsg= null;
        String description= null;
        Message inboundMsg =null;
        try{
            Guild g = event.getGuild();
            inboundMsg = event.getMessage();

            String contentRaw = inboundMsg.getContentRaw();

            if (checkEmoji(contentRaw)){
                String extractedEmoji = EmojiParser.extractEmojis(contentRaw).get(0);
                emoteMention = extractedEmoji;
                emoteName = extractedEmoji;
            }else{
                emoteMention = inboundMsg.getEmotes().get(0).getAsMention();
                emoteName = inboundMsg.getEmotes().get(0).getName().toLowerCase();
                if (event.getGuild().getEmoteById(inboundMsg.getEmotes().get(0).getId()) == null){
                    CMD_REACTION.negative(event);
                    return;
                }
            }

            role = inboundMsg.getMentionedRoles().get(0);
            roleId = role.getId();
            roleAsMention = role.getAsMention();
            reactionRoleMsg = g.getTextChannelById(guildConfig.getREACTION_ROLE_CHANNEL())
                    .retrieveMessageById(guildConfig.getREACTION_ROLE_MSG_ID()).complete();
            description = reactionRoleMsg.getEmbeds().get(0).getDescription();
        } catch (Exception e ){
            e.printStackTrace();
            CMD_REACTION.negative(event);
            //event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            return;
        }


        switch (args[0]){
            case "add":
                guildConfig.getREACTION_MAP().put(emoteName,roleId);
                StringBuilder sb = new StringBuilder();

                sb.append(description).append("\n").append(roleAsMention).append(" - ")
                        .append(emoteMention).append("\n");

                reactionRoleMsg.editMessage(new EmbedBuilder().setDescription(sb.toString()).build()).queue();
                if (checkEmoji(inboundMsg.getContentRaw())){
                    reactionRoleMsg.addReaction(emoteName).queue();
                }else{
                    reactionRoleMsg.addReaction(inboundMsg.getEmotes().get(0)).queue();
                }
                GuildConfig.saveConfig(guildConfig,event.getGuild().getId());
                CMD_REACTION.positive(event);
                break;
            case "remove":
                String replace = description.replace(roleAsMention + " - " + emoteMention, "");
                String replaceCleaned = replace.replaceAll("(?m)^[ \\t]*\\r?\\n", "");
                reactionRoleMsg.editMessage(new EmbedBuilder().setDescription(replaceCleaned).build()).queue();
                if (checkEmoji(inboundMsg.getContentRaw())){
                    reactionRoleMsg.removeReaction(emoteName).queue();
                }else {
                    reactionRoleMsg.removeReaction(event.getGuild().getEmotesByName(emoteName, true).get(0)).queue();
                }

                guildConfig.getREACTION_MAP().remove(emoteName);
                GuildConfig.saveConfig(guildConfig,event.getGuild().getId());
                CMD_REACTION.positive(event);
                break;

            default:
                event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
        }
    }
    private static boolean checkEmoji(String contentRaw){
        String s = EmojiParser.parseToAliases(contentRaw);
        List<String> s1 = Arrays.asList(s.split(" "));

        for (String str : s1) {
            if (str.trim().startsWith(":")) return true;
        }

        return false;
    }
    private static void setUp(MessageReceivedEvent event){
        /*List<Message> history = event.getTextChannel().getHistory().retrievePast(5).complete();
        history.forEach(msg->{
            msg.delete().queue();
        });*/
        System.out.println("settingUp");
        Guild g = event.getGuild();
        EmbedBuilder setUpEmbed = new EmbedBuilder();
        String introduction = "To get the role react to this msg with the correct emote\n";

        StringBuilder sb = new StringBuilder();
        List<Emote> emotes = new ArrayList<>();
        List<String> emojis = new ArrayList<>();
        AtomicReference<Role> role1 = new AtomicReference<>();
        System.out.println(guildConfig.getREACTION_MAP());
        if (guildConfig.getREACTION_MAP() == null){
            guildConfig.setREACTION_MAP(new HashMap<>());
        }
        guildConfig.getREACTION_MAP().forEach((k,v) -> {
            System.out.println(k + " -  "+v);
            if (checkEmoji(k)){
                role1.set(g.getRoleById(v));
                emojis.add(k);
                sb.append(role1.get().getAsMention()).append(" - ").append(k).append("\n");
            }else{
                role1.set(g.getRoleById(v));
                Emote emote = g.getEmotesByName(k,true).get(0);
                emotes.add(emote);
                sb.append(role1.get().getAsMention()).append(" - ").append(emote.getAsMention()).append("\n");
            }
        });

        MessageEmbed build = setUpEmbed.setDescription(introduction + sb.toString()).build();
        Message complete = event.getTextChannel().sendMessage(build).complete();

        guildConfig.setREACTION_ROLE_MSG_ID(complete.getId()) ;
        emotes.forEach(val -> {
            complete.addReaction(val).queue();
        });
        emojis.forEach(val -> {
            complete.addReaction(val).queue();
        });
        GuildConfig.saveConfig(guildConfig,event.getGuild().getId());
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
