package commands.community;

import commands.Command;
import commands.admin.Config;
import core.config.ConfigVals;
import core.config.GuildConfig;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.G_CONFIG;
import util.STATICS;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class Streamer implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        String guildID = event.getGuild().getId();

        ConfigVals configVals = STATICS.CONFIGS.get(guildID);

        if (!configVals.isSTREAMER_ENABLED()){

            return;
        }

        if (configVals.getSTREAM_ANNOUCMENT_CHANNEL().isEmpty()){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure the Announcement Channel for streams\n" +
                    "use " + configVals.getPREFIX() + " admin change stream #channel");

            event.getTextChannel().sendMessage(embedBuilder.build()).queue();
            return;
        }

        switch (args[0]){
            case "add":
            case "a":
                List<String> streamer_names = configVals.getSTREAMER_NAMES();
                if (streamer_names == null){
                    streamer_names = new ArrayList<>();
                }
                streamer_names.add(args[1]);
                configVals.setSTREAMER_NAMES(streamer_names);
                GuildConfig.saveConfig(configVals, guildID);
                CMD_REACTION.positive(event);
                G_CONFIG.streames();
                break;

            case "remove":
            case "rem":
                configVals.getSTREAMER_NAMES().remove(args[1]);
                GuildConfig.saveConfig(configVals, guildID);
                CMD_REACTION.positive(event);
                G_CONFIG.streames();
                break;
        }
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
