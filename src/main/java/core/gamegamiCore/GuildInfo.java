package core.gamegamiCore;

import core.models.GuildInfoModel;
import core.models.UniGuild;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class GuildInfo {

    public static GuildInfoModel getInfo(String guild, String id) throws IOException {
        //https://silkroad.gamegami.com/guild.php?shardid=2&guild=EIGHT

        String URL = "https://silkroad.gamegami.com/guild.php?shardid=2&guild="+guild;
        Document doc = Jsoup.connect(URL).get();
        String creationDate = doc.select("[id=gcreatedate]").text();
        String rank = doc.select("[id=grank]").text();
        String name = doc.select("[id=gname]").text();
        String memCount = doc.select("[id=guild_memcount_name]").text();
        String level = doc.select("[id=guild_level_name]").text();
        String points = doc.select("[id=guild_point_name]").text();
        String master = doc.select("[id=guild_master_name]").text();

        Element uniTableDic = doc.select("[class=table_rank]").get(1);
        Elements uniTable = uniTableDic.select("tbody");
        Elements rows = uniTable.select("tr");
        StringBuilder uniBuilder = new StringBuilder();
        ArrayList<UniGuild> union = new ArrayList<>();

        for (int i = 0; i < rows.size(); i++){
            Element row = rows.get(i);
            Elements cols = row.select("td");
            for (int a = 0; a<cols.size(); a++){
                union.add(new UniGuild(cols.get(a).text(),
                        "https://silkroad.gamegami.com/guild.php?shardid="+Servers.getID(id)+"&guild="+cols.get(a).text()));
                uniBuilder.append(cols.get(a).text());
                uniBuilder.append("\n");
            }
        }

        return new GuildInfoModel(creationDate,rank,name,memCount,level,points,master,union,URL);

    }
}
