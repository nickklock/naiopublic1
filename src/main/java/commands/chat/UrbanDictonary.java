package commands.chat;

import com.fasterxml.jackson.databind.ObjectMapper;
import commands.Command;
import core.config.ConfigVals;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.CMD_REACTION;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class UrbanDictonary implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isUD_ENABLED()) return;

        CMD_REACTION.positive(event);

        StringBuilder term = new StringBuilder();
        for (String arg :
                args) {
            term.append(arg);
        }
        JSONObject jsonObject = getJsonObject(term.toString());
        EmbedBuilder embedBuilder = createMessage(Objects.requireNonNull(jsonObject));
        event.getTextChannel().sendMessage(embedBuilder.setTitle("Urban definition for ["+term.toString()+"]").build()).queue();
    }

    private JSONObject getJsonObject(String term){
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://mashape-community-urban-dictionary.p.rapidapi.com/define?term=" + term)
                .get()
                .addHeader("x-rapidapi-host", "mashape-community-urban-dictionary.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "oL2VwxDGPImshKvpVoQ92eCPDI42p1e9FJ8jsnL0Aq7kwSPx7l")
                .build();

        try {
            Response response = client.newCall(request).execute();
            ResponseBody body = response.body();
            ObjectMapper objectMapper = new ObjectMapper();
            JSONObject jsonObject = objectMapper.readValue(body.string(), JSONObject.class);
            ArrayList<Object> arrayList = (ArrayList<Object>) jsonObject.get("list");
            JSONArray array = new JSONArray();

            array.addAll(arrayList);
            Object o = array.get(0);
            JSONObject finalObject = new JSONObject((Map) o);
            return finalObject;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private EmbedBuilder createMessage(JSONObject jo){
        String definition = jo.get("definition").toString();
        definition = definition.replaceAll("\\[","");
        definition = definition.replaceAll("\\]","");

        String example = jo.get("example").toString();
        example = example.replaceAll("\\[","");
        example = example.replaceAll("\\]","");
        return new EmbedBuilder().setDescription(definition).addField("Example",example,true).setColor(Color.ORANGE);
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
