package core.models;

public class FortressRankingModel {

    private String rank;
    private String name;
    private String kd;
    private String kdq;

    public FortressRankingModel(String rank, String name, String kd) {
        this.rank = rank;
        this.name = name;
        this.kd = kd;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKd() {
        return kd;
    }

    public void setKd(String kd) {
        this.kd = kd;
    }

    public String getKdq() {
        return kdq;
    }

    public void setKdq(String kdq) {
        this.kdq = kdq;
    }
}
