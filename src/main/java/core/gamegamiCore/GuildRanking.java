package core.gamegamiCore;

import core.models.GuildRank;
import net.dv8tion.jda.api.EmbedBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class GuildRanking {

    public static EmbedBuilder getEmbed(int part, String id){
        ArrayList<GuildRank> rankingsList = getGRanking(id);

        EmbedBuilder embedBuilder1 = new EmbedBuilder();
        embedBuilder1.setTitle("Guild Ranking (1/2)");
        embedBuilder1.setColor(Color.decode("#3c40c6"));
        switch (part){
            case 1:
                for (int i = 0; i< 24; i++){
                    GuildRank guildRank = rankingsList.get(i);
                    embedBuilder1.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "Points: "+guildRank.getPoints(),true);
                }
                return embedBuilder1;

            case 2:
                EmbedBuilder embedBuilder2 = new EmbedBuilder(embedBuilder1).setTitle("Guild Ranking (2/2)");
                for (int i = 25; i< rankingsList.size(); i++){
                    GuildRank guildRank = rankingsList.get(i);
                    embedBuilder2.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "Points: "+guildRank.getPoints(),true);
                }
                return embedBuilder2;
        }
        return null;
    }


    public static ArrayList<GuildRank> getGRanking(String ID){
        Document doc;

        try {
            doc = Jsoup.connect("https://silkroad.gamegami.com/ranking_guild.php?id="+ID)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .timeout(30*1000)
                    .get();
            Element guildTable = doc.select("tbody").get(0);
            Elements rows = guildTable.select("tr");
            ArrayList<GuildRank> ranks = new ArrayList<>();

            for (int i = 0; i < rows.size(); i++){
                Element row = rows.get(i);
                Elements cols = row.select("td");
                GuildRank gRank = new GuildRank(null,null,null);
                for (int a = 0; a < cols.size() ; a++){
                    switch (a){
                        case 1:
                            gRank.setRank(cols.get(a).text());
                            break;
                        case 2:
                            gRank.setName(cols.get(a).text());
                            break;
                        case 3:
                            gRank.setPoints(cols.get(a).text());
                            break;
                    }
                }
                if (gRank.getName() != null){
                    ranks.add(gRank);
                }
            }
            return ranks;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
