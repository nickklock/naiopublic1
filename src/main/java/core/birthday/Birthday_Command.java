package core.birthday;

import core.models.Bd;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Birthday_Command{
    public static void addBirthday(MessageReceivedEvent e, LocalDate birthday) throws IOException, ParseException, DuplicateName{

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("./guild_configs/"+e.getGuild().getId()+"_birthdays.json");
        Object obj = jsonParser.parse(reader);
        JSONObject jsonObject = (JSONObject) obj;
        if (jsonObject.containsKey(e.getMember().getId())) throw new DuplicateName(e.getMessage().getId());
        jsonObject.put(e.getMember().getId(), birthday.toString());

        FileWriter writer = new FileWriter("./guild_configs/"+e.getGuild().getId()+"_birthdays.json");
        writer.write(jsonObject.toJSONString());
        writer.flush();
    }

    public static void removeBirthday(String id, String gId) throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();

        FileReader reader = new FileReader("./guild_configs/"+gId+"_birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        obj.remove(id.trim());
        String s = JSONValue.toJSONString(obj);
        FileWriter writer = new FileWriter("./guild_configs/"+gId+"_birthdays.json");
        writer.write(s);
        writer.flush();
    }

    public static List<Bd> nextBirthdays(String gId) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("./guild_configs/"+gId+"_birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        LocalDate now = LocalDate.now();
        List<Bd> nextBDs = new ArrayList<>();
        obj.forEach((k,v) ->{
            LocalDate trueBd = LocalDate.parse(v.toString());
            Period until = trueBd.until(LocalDate.now());
            int yearsToAdd = until.getYears();
            Bd bd = new Bd();
            bd.setId(k.toString());
            bd.setBd(trueBd.plusYears(yearsToAdd));
            bd.setFutureAge(until.getYears()+1);

            nextBDs.add(bd);
        });
        Collections.sort(nextBDs,new SortByMonth());
        Collections.reverse(nextBDs);
        return nextBDs;
    }
    static class SortByMonth implements Comparator<Bd>{
        @Override
        public int compare(Bd a, Bd b) {
            return b.getBd().compareTo(a.getBd());
        }
    }

    public static ArrayList<Bd> getTodaysBirthdays(String gId) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("./guild_configs/"+gId+"_birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        LocalDate now = LocalDate.now();
        ArrayList<Bd> todaysBD = new ArrayList<>();
        obj.forEach((k,v) ->{
            Bd bd = new Bd();
            bd.setId(k.toString());
            bd.setBd(LocalDate.parse(v.toString()));
            LocalDate bdToCheck = bd.getBd();
            if (bdToCheck.getMonthValue() == now.getMonthValue() && bdToCheck.getDayOfMonth() == now.getDayOfMonth()){
                todaysBD.add(bd);
            }
        });
        return todaysBD;
    }
}
