package commands.admin;

import commands.Command;
import core.config.ConfigVals;
import core.config.GuildConfig;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.G_CONFIG;
import util.POWER;

import java.io.IOException;
import java.text.ParseException;

public class Admin implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        if (POWER.noPower(event.getMember())){
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage(EMBEDS.nopowerEmbed().build()).queue();
            return;
        }

        if (args.length < 1){
            CMD_REACTION.positive(event);
            event.getTextChannel().sendTyping().queue();
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            return;
        }
        String guildID = event.getGuild().getId();
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(guildID);
        switch (args[0]){
            case "disable":
            case "enable":
                switch (args[1]){
                    case "clear":
                        guildConfig.setCLEAR_ENABLED(!guildConfig.isCLEAR_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "ud":
                        guildConfig.setUD_ENABLED(!guildConfig.isUD_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "ui":
                        guildConfig.setUI_ENABLED(!guildConfig.isUI_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "bd":
                        guildConfig.setBD_ENABLED(!guildConfig.isBD_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "gg":
                        guildConfig.setGG_ENABLED(!guildConfig.isGG_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "raffle":
                        guildConfig.setRAFFLE_ENABLED(!guildConfig.isRAFFLE_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "streamer":
                        guildConfig.setSTREAMER_ENABLED(!guildConfig.isRAFFLE_ENABLED());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                }
                break;

            case "change":
                TextChannel mentionedChannel = null;
                if (event.getMessage().getMentionedChannels().size() > 0){
                     mentionedChannel = event.getMessage().getMentionedChannels().get(0);
                }
                switch (args[1]){
                    case "raffle":
                        guildConfig.setRAFFLE_CHANNEL(mentionedChannel.getId());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "birthday":

                        guildConfig.setBD_CHANNEL(mentionedChannel.getId());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "ggserver":
                        guildConfig.setGG_SERVER(args[2]);
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;

                    case "ggranking":
                        guildConfig.setGG_RANKING_CHANNEL(mentionedChannel.getId());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;

                    case "prefix":
                        guildConfig.setPREFIX(args[2]);
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "stream":
                        guildConfig.setSTREAM_ANNOUCMENT_CHANNEL(mentionedChannel.getId());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                    case "reactionrole":
                        guildConfig.setREACTION_ROLE_CHANNEL(mentionedChannel.getId());
                        GuildConfig.saveConfig(guildConfig, guildID);
                        CMD_REACTION.positive(event);
                        break;
                }
                break;

            case "power":
                String mentionedRoleName = event.getMessage().getMentionedRoles().get(0).getName();
                guildConfig.getPOWER_ROLES().add(mentionedRoleName);
                GuildConfig.saveConfig(guildConfig, guildID);
                CMD_REACTION.positive(event);
                break;
        }
    }

    @Override
    public String help(){
        return "enable/disable [clear/ud/ui/raffle/gg/bd]\nchange [raffle/birthday/ggserver]";
    }

    @Override
    public String description(){
        return "To change the settings of your bot. Or to add roles with admin powers";
    }
}
