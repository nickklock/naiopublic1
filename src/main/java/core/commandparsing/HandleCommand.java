package core.commandparsing;

import util.STATICS;

import java.io.IOException;
import java.text.ParseException;

public class HandleCommand{

    public static void handleCommand(CommandContainer cmd) throws ParseException, IOException{

        if (STATICS.COMMANDS.containsKey(cmd.invoke.toLowerCase())) {
            STATICS.COMMANDS.get(cmd.invoke.toLowerCase()).action(cmd.args, cmd.event);
        }
    }
}
