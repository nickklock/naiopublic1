package commands.support;

import commands.Command;
import core.config.GuildConfig;
import core.config.Premium;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

public class Owner implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        if (!event.getAuthor().getId().equals("151681624436768768")) return;

        switch (args[0]){
            case "createDefault":
                G_CONFIG.getGuildConfig(event.getGuild().getId());
                GuildConfig.saveDefault(G_CONFIG.getGuildConfig(event.getGuild().getId()),event.getGuild().getId());
                break;
            case "allG":
                StringBuilder stringBuilder = new StringBuilder();
                for (Guild g :
                        STATICS.JDA.getGuilds()) {
                    stringBuilder.append(g.getName()).append(" - ").append(g.getId()).append("\n");
                }
                CMD_REACTION.positive(event);
                event.getTextChannel().sendMessage(stringBuilder.toString()).queue();
                break;

            case "pmOwners":
                List<String> textList = Arrays.asList(args).subList(1,args.length);
                StringBuilder stringBuilder1 = new StringBuilder();

                for (String s :
                        textList) {
                    stringBuilder1.append(s).append(" ");
                }
                EmbedBuilder embedBuilder = new EmbedBuilder().setTitle("Announcement from the dev.")
                        .setColor(Color.CYAN)
                        .setDescription(stringBuilder1.toString())
                        ;
                CMD_REACTION.positive(event);
                for (Guild g :
                        STATICS.JDA.getGuilds()) {
                    System.out.println(g.getName()+" - "+g.retrieveOwner().complete().getUser().getName());
                    PrivateChannel pmOwner = g.retrieveOwner().complete().getUser().openPrivateChannel()
                            .complete();
                    pmOwner.sendMessage(embedBuilder.build()).queue();
                    pmOwner.close().queue();
                }
                break;

            case "addPrem":
                Premium.addPrem(args[1]);
                CMD_REACTION.positive(event);

                break;
        }
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
