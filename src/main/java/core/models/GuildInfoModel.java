package core.models;

import java.util.ArrayList;

public class GuildInfoModel {
    private String creationDate ;
    private String rank  ;
    private String name ;
    private String memCount ;
    private String level ;
    private String points ;
    private String master ;
    private ArrayList<UniGuild> union;
    private String URL;

    public GuildInfoModel(String creationDate, String rank, String name, String memCount,
                          String level, String points, String master, ArrayList<UniGuild> union, String URL) {
        this.creationDate = creationDate;
        this.rank = rank;
        this.name = name;
        this.memCount = memCount;
        this.level = level;
        this.points = points;
        this.master = master;
        this.union = union;
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemCount() {
        return memCount;
    }

    public void setMemCount(String memCount) {
        this.memCount = memCount;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public ArrayList<UniGuild> getUnion() {
        return union;
    }

    public void setUnion(ArrayList<UniGuild> union) {
        this.union = union;
    }
}
