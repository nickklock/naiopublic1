package util;

import core.config.ConfigVals;

import java.util.ArrayList;
import java.util.List;

public class G_CONFIG{

    public static ConfigVals getGuildConfig(String guildID){

        return STATICS.CONFIGS.get(guildID);
    }

    public static void streames(){
        List<String> streamingChannels = new ArrayList<>();
        STATICS.CONFIGS.forEach((id,config) -> {
            if (config.getSTREAMER_NAMES() != null){
                streamingChannels.addAll(config.getSTREAMER_NAMES());
            }
        });

        STATICS.TWITCH.getClientHelper().enableStreamEventListener(streamingChannels);
    }
}
