package core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import util.STATICS;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GuildConfig{
    public static void readGuildConfigs(){

        try {
            File folder = new File("./guild_configs");
            File[] listOfFiles = folder.listFiles();
            for (File configFile : listOfFiles) {
                if (configFile.getName().contains("_config")){
                    String[] id = configFile.getName().split("_");
                    ConfigVals cv = new ObjectMapper()
                            .readValue(new String(Files.readAllBytes(Paths.get(configFile.getPath()))), ConfigVals.class);
                    cv.setGUILD_ID(id[0]);
                    STATICS.CONFIGS.put(id[0],cv);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveConfig(ConfigVals cv, String gId) {
        ObjectMapper om = new ObjectMapper();
        try {
            Path originalPath =  Paths.get("./guild_configs/"+gId+"_config.json");
            Files.delete(originalPath);
            Files.write(originalPath,om.writeValueAsBytes(cv));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveDefault(ConfigVals cv, String gId) {
        ObjectMapper om = new ObjectMapper();
        try {
            Path originalPath =  Paths.get("./default_config.json");
            Files.delete(originalPath);
            Files.write(originalPath,om.writeValueAsBytes(cv));
            System.out.println(om.writeValueAsString(cv));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
