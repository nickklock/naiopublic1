package core.models;

public class PlayerInfo {
    private String guild;
    private String race;
    private String alias;
    private String honor;
    private String title;
    private String charName;
    private String points ;
    private String level;
    private String hp;
    private String mp ;
    private String strInt ;
    private String url;
    private String thumb;
    private String lastestGlobal;
    private String latestUniqueKill;
    private String set;

    public String getSet(){
        return set;
    }

    public void setSet(String set){
        this.set = set;
    }

    public String getLastestGlobal() {
        return lastestGlobal;
    }

    public void setLastestGlobal(String lastestGlobal) {
        this.lastestGlobal = lastestGlobal;
    }

    public String getLatestUniqueKill() {
        return latestUniqueKill;
    }

    public void setLatestUniqueKill(String latestUniqueKill) {
        this.latestUniqueKill = latestUniqueKill;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getMp() {
        return mp;
    }

    public void setMp(String mp) {
        this.mp = mp;
    }

    public String getStrInt() {
        return strInt;
    }

    public void setStrInt(String strInt) {
        this.strInt = strInt;
    }

    public PlayerInfo(String guild, String race, String alias, String honor, String title,
                      String charName, String points, String level, String hp, String mp, String strInt) {
        this.guild = guild;
        this.race = race;
        this.alias = alias;
        this.honor = honor;
        this.title = title;
        this.charName = charName;
        this.points = points;
        this.level = level;
        this.hp = hp;
        this.mp = mp;
        this.strInt = strInt;
    }

    public String getGuild() {
        return guild;
    }

    public void setGuild(String guild) {
        this.guild = guild;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getHonor() {
        return honor;
    }

    public void setHonor(String honor) {
        this.honor = honor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
