package core.application;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.STATICS;

import javax.annotation.Nonnull;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ApplicationListener extends ListenerAdapter{
    public static boolean active ;
    public static int step ;
    public static LocalDateTime timeCreated;


    static List<String> answers = new ArrayList<>();
    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (event.getChannel().getType() != ChannelType.PRIVATE) return;
        if (event.getAuthor() == event.getJDA().getSelfUser())return;

        System.out.println("here");
        Message inboundMsg = event.getMessage();
        String answer = inboundMsg.getContentRaw();
        Timer timer = endAfter5Min(event);
        if (answer.toLowerCase().equals("n")){
            event.getPrivateChannel().close().queue();
            step = 0;
            active = false;
            answers.removeAll(answers);
            STATICS.Currentapplicants.remove(event.getAuthor().getId());
            timer.cancel();
            return;
        }

        EmbedBuilder answerBed = new EmbedBuilder();
        switch (step){
            case 0:
                if (answer.toLowerCase().equals("y")){
                    timeCreated = LocalDateTime.now();
                    answers.add(event.getMessage().getContentRaw());
                    answerBed.setDescription("What's your Name?");
                    event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                    step++;
                }
                break;
            case 1:
                answers.add(event.getMessage().getContentRaw());
                answerBed.setDescription("What's your Charname?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 2:
                answers.add(event.getMessage().getContentRaw());
                answerBed.setDescription("What's your Discord Tag?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 3:
                answers.add(answer);
                answerBed.setDescription("How old are you?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 4:
                answers.add(answer);
                answerBed.setDescription("Where are u from?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 5:
                answers.add(answer);
                answerBed.setDescription("What motivates you in Silkroad? ");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 6:
                answers.add(answer);
                answerBed.setDescription("What Guild(s) have you been in?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 7:
                answers.add(answer);
                answerBed.setDescription("Why did you leave those?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;
            case 8:
                answers.add(answer);
                answerBed.setDescription("What sets you apart from other players?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;

            case 9:
                answers.add(answer);
                answerBed.setDescription("Why do you want to join EIGHT?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;

            case 10:
                answers.add(answer);
                answerBed.setDescription("Do you know anyone in EIGHT? Or is there anyone in EIGHT that could vouch" +
                        " for you as a player? If yes who?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;

            case 11:
                answers.add(answer);
                answerBed.setDescription("Do you know anyone in EIGHT? Or is there anyone in EIGHT that could vouch" +
                        " for you as a player? If yes who?");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step++;
                break;

            case 12:
                answers.add(event.getMessage().getContentRaw());
                step = 24;
                answerBed.setDescription("Alright you are done for now. I'll forward your info to the deputies. " +
                        "You'll hear from us soon.");
                event.getPrivateChannel().sendMessage(answerBed.build()).queue();
                step = 0;
                active = false;
                finalMsg(event);
                answers.removeAll(answers);
                STATICS.Currentapplicants.remove(event.getAuthor().getId());
                timer.cancel();
                break;


        }


    }

    private static void finalMsg(MessageReceivedEvent e){
        EmbedBuilder embed  = new EmbedBuilder();
        embed.setAuthor(e.getAuthor().getName(),null,e.getAuthor().getEffectiveAvatarUrl());
        embed.setTitle("New Application");
        embed.setColor(Color.decode("#45aaf2"));
        embed.addField("What's your Name?",answers.get(1),false);
        embed.addField("What's your Charname?",answers.get(2),false);
        embed.addField("What's your Discord Tag?",answers.get(3),false);
        embed.addField("Where are you from?",answers.get(5),false);
        embed.addField("How old are you?",answers.get(4),false);
        embed.addField("What motivates you in Silkroad?",answers.get(6),false);
        embed.addField("What Guild(s) have you been in? ",answers.get(7),false);
        embed.addField("Why did you leave those?",answers.get(8),false);
        embed.addField("What sets you apart from other players?",answers.get(9),false);
        embed.addField("Why do you want to join EIGHT?",answers.get(10),false);
        embed.addField("Do you know anyone in EIGHT? Or is there anyone in EIGHT that could vouch for you as a player?",answers.get(11),false);

        e.getJDA().getGuildById(STATICS.EIGHT_GUILD_ID).getTextChannelById(STATICS.EIGHT_APPLYCHANNEL_ID).
                sendMessage(embed.build()).queue();
    }

    private Timer endAfter5Min(MessageReceivedEvent event){
        TimerTask task = new TimerTask() {
            public void run() {
                step = 0;
                active = false;
                answers.removeAll(answers);
                STATICS.Currentapplicants.remove(event.getAuthor().getId());
            }
        };
        Timer timer = new Timer();

        timer.schedule(task,300000);
        return timer;
    }
}
