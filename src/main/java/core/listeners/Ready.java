package core.listeners;

import core.birthday.Birthday_Task;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Ready extends ListenerAdapter{

    @Override
    public void onReady(@Nonnull ReadyEvent event){
        initializeBirthdayTask();
    }

    private static void initializeBirthdayTask(){
        System.out.println("initializing birthday reminder...");
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        long midnight= LocalDateTime.now().until(LocalDate.now().plusDays(1).atStartOfDay().plusMinutes(2), ChronoUnit.MINUTES);
        //long now = LocalDateTime.now().until(LocalDateTime.now().plusSeconds(20),ChronoUnit.MINUTES);
        scheduler.scheduleAtFixedRate(new Birthday_Task(), midnight, TimeUnit.DAYS.toMinutes(1), TimeUnit.MINUTES);

        System.out.println("initializing birthday reminder done");

        System.out.println("ready");


    }
}
