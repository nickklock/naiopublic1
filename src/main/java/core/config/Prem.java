package core.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Prem{
    private List<String> ids;

    public Prem(List<String> ids){
        this.ids = ids;
    }

    public Prem(){
    }

    public List<String> getIds(){
        return ids;
    }

    public void setId(List<String> ids){
        this.ids = ids;
    }
}
