package commands.community;

import commands.Command;
import core.CustomTriggers;
import core.config.ConfigVals;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.G_CONFIG;
import util.POWER;

import java.io.IOException;
import java.text.ParseException;

public class Triggers implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isTR_ENABLED()) return;

        if (args.length < 1){
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            CMD_REACTION.negative(event);
            return;
        }

        String identifier = args[0] ;
        StringBuilder allArgs = new StringBuilder();
        for(int i = 1 ; i < args.length ; i++){
            allArgs.append(args[i]).append(" ");
        }

        switch (identifier){
            case "add":
                String[] split = allArgs.toString().split("\\|");
                String trigger = split[0].trim();
                String response = split[1].trim();

                try {
                    CustomTriggers.addTrigger(trigger, response,event.getGuild().getId());
                    CMD_REACTION.positive(event);
                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);

                }
                break;

            case "all":
                PrivateChannel privateChannel = event.getMember().getUser().openPrivateChannel().complete();

                try {
                    String allTriggers = CustomTriggers.getAllTriggers(event.getGuild().getId());
                    CMD_REACTION.positive(event);
                    privateChannel.sendMessage(allTriggers).queue();
                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);
                }

                break;
            case "remove":
                try {
                    CustomTriggers.delteTrigger(allArgs.toString(),event.getGuild().getId());
                    CMD_REACTION.positive(event);

                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);

                }
                break;

        }
    }

    @Override
    public String help(){
        return  "**.t / .triggers add** *some trigger* | *some response*\n" +
                "**.t / .triggers remove** *some trigger*\n"+
                "**.t/ .triggers all**";
    }

    @Override
    public String description(){
        return "Manages the custom responses. You can add, remove and get a list of all triggers.";
    }
}
