package core.commandparsing;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CommandContainer{

    public final String raw;
    public final String beheaded;
    public final String[] splitBeheaded;
    public final String invoke;
    public final String[] args;
    public final MessageReceivedEvent event;

    public CommandContainer(String rw, String beheaded, String[] splitBeheaded, String invoke, String[] args, MessageReceivedEvent e) {
        this.raw = rw;
        this.beheaded = beheaded;
        this.splitBeheaded = splitBeheaded;
        this.invoke = invoke;
        this.args = args;
        this.event = e;
    }
}
