package commands.admin;

import commands.Command;
import core.config.ConfigVals;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;

public class Naio implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        CMD_REACTION.positive(event);
        int amountOfGuilds = STATICS.JDA.getGuilds().size();
        int amountOfCommands = STATICS.COMMANDS.size();

        double gatewayPing = STATICS.JDA.getGatewayPing();
        Long restPing = STATICS.JDA.getRestPing().complete();
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        EmbedBuilder embedBuilder = new EmbedBuilder().setColor(Color.CYAN).setTitle("NAIO Info")
                .addField("Version",STATICS.VERSION,true)
                .addField("Gateway Ping",String.valueOf(gatewayPing)+ "ms",true)
                .addField("Rest Ping",String.valueOf(restPing)+" ms",true)
                .addField("Guilds im in",String.valueOf(amountOfGuilds),true)
                .addField("Ammount of commands",String.valueOf(amountOfCommands),true)
                .addBlankField(true)
                .addField("Invite Link","[Link](https://discord.com/api/oauth2/authorize?client_id=785908278298935306&permissions=1475865712&scope=bot)",true)
                .addField("Support Server","[Link](https://discord.gg/8DyUsVrrcJ)",true)
                .addBlankField(true)
                ;

        embedBuilder.setDescription("**Commands**\n``"+
                guildConfig.getPREFIX()+"raffle - Creates a list from Names and raffles a winner\n"+
                guildConfig.getPREFIX()+"t - Add/remove and list all custom triggers\n"+
                guildConfig.getPREFIX()+"bd - Add/remove you birthday from the reminder system\n"+
                guildConfig.getPREFIX()+"streamer - Add/remove a streamer for announcements\n"+
                guildConfig.getPREFIX()+"rr - Add/remove a role to the reaction role system\n"+
                guildConfig.getPREFIX()+"gg - Playerinfo/Guildinfo for GameGami servers\n"+
                guildConfig.getPREFIX()+"clear - Clears x messages from a text-channel\n"+
                guildConfig.getPREFIX()+"ud - Urban Dictionary\n" +
                guildConfig.getPREFIX()+"config - Displays the current config\n" +
                guildConfig.getPREFIX()+"admin - To change the configuration\n" +
                guildConfig.getPREFIX()+"help - Shows this message\n" +
                guildConfig.getPREFIX()+"userinfo - Displays user related infos``" +
                "\n\n _To get more info just type the command_");

        event.getTextChannel().sendTyping().queue();
        event.getTextChannel().sendMessage(embedBuilder.build()).queue();
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
