package commands.admin;

import commands.Command;
import core.config.ConfigVals;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import util.CMD_REACTION;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;

public class Config implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        EmbedBuilder embedBuilder = new EmbedBuilder().setColor(Color.CYAN).setTitle("My Config for "+event.getGuild().getName().toUpperCase());
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        String premium;
        if (STATICS.PREMIUM.contains(event.getGuild().getId())){
            premium = "true";
        }else{
            premium = "false";
        }
        String bdChannel;
        if (guildConfig.getBD_CHANNEL().isEmpty()){
            bdChannel = "null";
        }else{
            bdChannel = event.getGuild().getTextChannelById(guildConfig.getBD_CHANNEL()).getAsMention();
        }

        String raffleChannel;
        if (guildConfig.getRAFFLE_CHANNEL().isEmpty()){
            raffleChannel = "null";
        }else {
            raffleChannel = event.getGuild().getTextChannelById(guildConfig.getRAFFLE_CHANNEL()).getAsMention();
        }

        String rrChannel;
        if (guildConfig.getREACTION_ROLE_CHANNEL().isEmpty()){
            rrChannel = "null";
        }else {
            rrChannel = event.getGuild().getTextChannelById(guildConfig.getREACTION_ROLE_CHANNEL()).getAsMention();
        }

        String streamChannel;
        if (guildConfig.getSTREAM_ANNOUCMENT_CHANNEL().isEmpty()){
            streamChannel = "null";
        }else {
            streamChannel = event.getGuild().getTextChannelById(guildConfig.getSTREAM_ANNOUCMENT_CHANNEL()).getAsMention();
        }
        String rrurl = "null";
        try {
            if (!guildConfig.getREACTION_ROLE_CHANNEL().isEmpty() && !guildConfig.getREACTION_ROLE_MSG_ID().isEmpty()){
                Message rrMsg = event.getGuild().getTextChannelById(guildConfig.getREACTION_ROLE_CHANNEL())
                        .retrieveMessageById(guildConfig.getREACTION_ROLE_MSG_ID()).complete();

                if (rrMsg == null){
                    rrurl = "null";
                }else {
                    rrurl ="[Click]("+rrMsg.getJumpUrl()+")";
                }
            }

        }catch (ErrorResponseException e){
            rrurl = "null";
        }

        String ggurl = "null";

        try {
            if (!guildConfig.getGG_RANKING_CHANNEL().isEmpty() && !guildConfig.getGG_RANKING_MSG_ID().isEmpty() ){
                Message ggMsg = event.getGuild().getTextChannelById(guildConfig.getGG_RANKING_CHANNEL())
                        .retrieveMessageById(guildConfig.getGG_RANKING_MSG_ID()).complete();

                if (ggMsg == null){
                    ggurl = "null";
                }else {
                    ggurl ="[Click]("+ggMsg.getJumpUrl()+")";
                }
            }

        }catch (ErrorResponseException e){
            ggurl = "null";
        }


        embedBuilder.addField("Clear enabled",String.valueOf(guildConfig.isCLEAR_ENABLED()),true);
        embedBuilder.addField("UD enabled",String.valueOf(guildConfig.isUD_ENABLED()),true);
        embedBuilder.addField("UI enabled",String.valueOf(guildConfig.isUI_ENABLED()),true);

        embedBuilder.addField("Birthday enabled",String.valueOf(guildConfig.isBD_ENABLED()),true);
        embedBuilder.addField("Triggers enabled",String.valueOf(guildConfig.isTR_ENABLED()),true);
        embedBuilder.addField("GameGami enabled",String.valueOf(guildConfig.isGG_ENABLED()),true);

        embedBuilder.addField("Raffle enabled",String.valueOf(guildConfig.isRAFFLE_ENABLED()),true);
        embedBuilder.addField("Stream enabled",String.valueOf(guildConfig.isSTREAMER_ENABLED()),true);
        embedBuilder.addBlankField(true);

        embedBuilder.addField("Birthday channel",bdChannel,true);
        embedBuilder.addField("Raffle channel",raffleChannel,true);
        embedBuilder.addField("Stream channel",streamChannel,true);

        embedBuilder.addField("Reaction Role channel",rrChannel,true);
        embedBuilder.addField("Reaction Role Msg",rrurl,true);
        embedBuilder.addBlankField(true);

        embedBuilder.addField("Streamer List",String.valueOf(guildConfig.getSTREAMER_NAMES()),true);
        embedBuilder.addField("GameGami Server",String.valueOf(guildConfig.getGG_SERVER()),true);
        embedBuilder.addField("GameGami Ranking Msg",ggurl,true);

        embedBuilder.addField("Premium guild",premium,true);
        embedBuilder.addBlankField(true);
        embedBuilder.addBlankField(true);



        CMD_REACTION.positive(event);
        event.getTextChannel().sendTyping().queue();
        event.getTextChannel().sendMessage(embedBuilder.build()).queue();
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
