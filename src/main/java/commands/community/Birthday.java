package commands.community;

import commands.Command;
import core.config.ConfigVals;
import core.models.Bd;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import util.CMD_REACTION;
import util.EMBEDS;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static core.birthday.Birthday_Command.addBirthday;
import static core.birthday.Birthday_Command.nextBirthdays;
import static core.birthday.Birthday_Command.removeBirthday;
import static util.EMBEDS.helpEmbed;

public class Birthday implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isBD_ENABLED()) return;
        if (args.length < 1){
            event.getTextChannel().sendMessage(helpEmbed(help(),description()).build()).queue();
            CMD_REACTION.negative(event);
            return;
        }

        if (guildConfig.getBD_CHANNEL().isEmpty()){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure the Announcement Channel for birthdays\n" +
                    "use " + guildConfig.getPREFIX() + " admin change birthday #channel");

            event.getTextChannel().sendMessage(embedBuilder.build()).queue();
            return;
        }
        String identifier = args[0];

        switch (identifier){
            case "add":
                try {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                    formatter = formatter.withLocale( Locale.GERMAN );
                    LocalDate birthday = LocalDate.parse(args[1], formatter);
                    addBirthday(event,birthday);
                    CMD_REACTION.positive(event);
                } catch (org.json.simple.parser.ParseException | DuplicateName e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);
                }
                return;
            case "remove":
                try {
                    removeBirthday(event.getAuthor().getId(),event.getGuild().getId());
                    CMD_REACTION.positive(event);
                    return;
                } catch (org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);
                }
                break;
            case "next":
                try {
                    StringBuilder birthdays = new StringBuilder();
                    CMD_REACTION.positive(event);
                    for (Bd b : nextBirthdays(event.getGuild().getId())) {
                        Period until = b.getBd().until(LocalDate.now());
                        int age = b.getFutureAge();
                        birthdays.append("<@"+b.getId()+">").append(" - ")
                                .append(b.getBd().getDayOfMonth()).append(".").
                                append(b.getBd().getMonthValue()).append(" (").append(age).append(")\n");
                    }
                    EmbedBuilder eb = new EmbedBuilder().setColor(Color.LIGHT_GRAY).setTitle("NEXT BIRTHDAYS").setDescription(birthdays.toString());
                    event.getTextChannel().sendMessage(eb.build()).queue();
                } catch ( org.json.simple.parser.ParseException e) {
                    e.printStackTrace();
                    CMD_REACTION.negative(event);
                    return;
                }
                break;

        }
    }

    @Override
    public String help(){
        return "**.bd add** 1.1.1990\n" +
                "**.bd next**\n" +
                "**.bd remove**";
    }

    @Override
    public String description(){
        return "Announces Birthdays. You can add and remove your Birthday";
    }
}
