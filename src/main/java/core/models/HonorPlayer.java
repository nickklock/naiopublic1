package core.models;

public class HonorPlayer {

    private int rank;
    private String player;
    private int graduates;
    private String guild;

    public HonorPlayer(int rank, String player, int graduates, String guild) {
        this.rank = rank;
        this.player = player;
        this.graduates = graduates;
        this.guild = guild;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getGraduates() {
        return graduates;
    }

    public void setGraduates(int graduates) {
        this.graduates = graduates;
    }

    public String getGuild() {
        return guild;
    }

    public void setGuild(String guild) {
        this.guild = guild;
    }
}
