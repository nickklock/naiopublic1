package commands.sro;

import commands.Command;
import core.config.ConfigVals;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.G_CONFIG;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class Raffle implements Command{
    private static LinkedList participants;
    private static LinkedList participants2;
    private static boolean multi;
    private static ArrayList<String> winners;

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isRAFFLE_ENABLED()) return;

        if (guildConfig.getRAFFLE_CHANNEL() == null || guildConfig.getRAFFLE_CHANNEL().equals("")){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure the channel for raffles\n" +
                    "use " + guildConfig.getPREFIX() + " admin change raffle #channel");
            event.getTextChannel().sendMessage(embedBuilder.build()).queue();
            return;
        }

        String raffle_channel = guildConfig.getRAFFLE_CHANNEL();

        if (!event.getTextChannel().getId().equals(raffle_channel)){
            String asMention = Objects.requireNonNull(event.getGuild().getTextChannelById(raffle_channel)).getAsMention();
            event.getTextChannel().sendMessage(EMBEDS.errorEmbed("Wrong text-channel",
                    " Only usable in "+asMention+" .").build()).queue();
            CMD_REACTION.negative(event);
            return;
        }
        if (args.length < 1){
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            CMD_REACTION.negative(event);
            return;
        }

        participants = new LinkedList<>(Arrays.asList(args));
        participants2 = new LinkedList<>(Arrays.asList(args));
        winners = new ArrayList<>();
        CMD_REACTION.positive(event);
        switch (String.valueOf(isInt(args[0]))){
            case "true":
                int howMany = Integer.valueOf(args[0]);
                participants.remove(args[0]);
                participants2.remove(args[0]);
                multi = true;

                if (howMany > participants.size()){
                    System.out.println("more times then participants");
                    System.out.println(participants2);
                    int leftoverTimes = howMany - participants.size();

                    for (int i = 1; i <= howMany; i++){
                        EmbedBuilder message = createMessage();
                        if (message != null){
                            event.getTextChannel().sendMessage(message.build()).queue();
                        }
                    }

                    participants = participants2;
                    System.out.println(participants2);
                    for (int i = 1; i <= leftoverTimes; i++){
                        EmbedBuilder message = createMessage();
                        if (message != null) {
                            event.getTextChannel().sendMessage(message.build()).queue();
                        }
                    }
                    multi = false;

                }else{
                    for (int i = 1; i <= howMany; i++){
                        event.getTextChannel().sendMessage(createMessage().build()).queue();
                    }
                    multi = false;
                }
                PrivateChannel privateChannel = event.getAuthor().openPrivateChannel().complete();
                privateChannel.sendMessage(createPM().build()).queue();
                privateChannel.close().queue();
                break;


            case "false":
                if (participants.size() < 23) {
                    event.getTextChannel().sendMessage(createMessage().build()).queue();
                }else{
                    event.getTextChannel().sendMessage(createMessage().build()).queue();
                }
                PrivateChannel privateChannel2 = event.getAuthor().openPrivateChannel().complete();
                privateChannel2.sendMessage(createPM().build()).queue();
                privateChannel2.close().queue();

                break;
        }

    }

    private static EmbedBuilder createMessage(){
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.LLLL.yyyy HH:mm");
        String formattedString = localDateTime.format(formatter);
        StringBuilder list = new StringBuilder();

        for (int a = 0 ; a < participants.size(); a++){
            int ii = a+1;
            list.append("#").append(ii).append(" - ").append(participants.get(a)).append("\n");
        }

        EmbedBuilder msg = new EmbedBuilder()
                .setColor(Color.decode("#3c40c6"))
                .setTitle("Participants")
                .setFooter("Raffle date: "+formattedString,null)
                .setDescription(list);

        msg.addBlankField(false);

        if (participants.size() == 0) return null ;
        int randomNum = ThreadLocalRandom.current().nextInt(0, participants.size() );
        msg.addField("WINNER","Number: "+(randomNum+1) + " | Name: "+participants.get(randomNum),false);
        winners.add(participants.get(randomNum).toString());
        if (multi) {
            participants.remove(randomNum);
        }


        return msg;
    }

    private static EmbedBuilder createPM(){
        StringBuilder winner = new StringBuilder();

        for (String a :winners) {
            winner.append("\n").append(a);
        }

        return new EmbedBuilder().setColor(Color.GREEN).setDescription(winner.toString());
    }

    private static boolean isInt(String a){
        try{
            int b = Integer.valueOf(a);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
