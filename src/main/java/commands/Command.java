package commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.io.IOException;
import java.text.ParseException;

public interface Command{

    void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException;
    String help();
    String description();
}
