package core.models;

public class GuildRank {
    private String rank;
    private String name;
    private String points;

    public GuildRank(String rank, String name, String points) {
        this.rank = rank;
        this.name = name;
        this.points = points;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public String getName() {
        return name;
    }

    public String getPoints() {
        return points;
    }
}
