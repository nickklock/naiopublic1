package util;

import core.config.ConfigVals;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.List;

public class POWER{
    public static boolean noPower(Member m){

        List<Role> mRoles = m.getRoles();
        if (m.isOwner() ) return false;
        if (m.getUser().getId().equals("151681624436768768"))return false;
        if (m.hasPermission(Permission.ADMINISTRATOR))return false;
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(m.getGuild().getId());
        List<Object> power_roles = guildConfig.getPOWER_ROLES();
        for (Role r :
                mRoles) {
            if (power_roles.contains(r.getName().toLowerCase())){
                System.out.println(false);
                return false;
            }
        }

        return true;
    }
}
