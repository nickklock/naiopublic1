package core;

import com.github.philippheuer.events4j.simple.SimpleEventHandler;
import com.github.twitch4j.TwitchClientBuilder;
import commands.Command;
import commands.admin.*;
import commands.chat.Clear;
import commands.chat.UrbanDictonary;
import commands.chat.Userinfo;
import commands.community.Birthday;
import commands.community.ReactionRole;
import commands.community.Streamer;
import commands.community.Triggers;
import commands.sro.Apply;
import commands.sro.Balance;
import commands.sro.GameGami;
import commands.sro.Raffle;
import commands.support.Owner;
import commands.support.Ticket;
import core.StreamingCore.DiscordStreaming;
import core.StreamingCore.TwitchStreaming;
import core.application.ApplicationListener;
import core.application.ApplicationReaction;
import core.config.GuildConfig;
import core.config.Premium;
import core.listeners.*;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import util.G_CONFIG;
import util.STATICS;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NAIO4{

    public static HashMap<String, Command> commands = new HashMap<>();
    private static JDABuilder shardBuilder;

    public static void main(String[] args){
        System.out.println("initializing bot...");
        try {
            setUp();
        } catch (LoginException e) {
            e.printStackTrace();
        }
        System.out.println("initializing bot done");

    }

    private static void setUp() throws LoginException{
        System.out.println("initializing jda...");
        shardBuilder = JDABuilder.createDefault(STATICS.TOKEN)
                .setActivity(Activity.streaming(STATICS.VERSION,
                        "https://www.twitch.tv/justFame01"))
                .setAutoReconnect(true);

        shardBuilder.useSharding(0, 1);

        GuildConfig.readGuildConfigs();
        initializeListeners();
        initializeCommands();
        getPremGuilds();
        STATICS.JDA = shardBuilder.build();
        createtwitchClient();
        System.out.println("initializing jda done");


    }

    private static void initializeListeners(){
        System.out.println("initializing listeners...");
        shardBuilder.addEventListeners(new Commands(), new BotJoin(), new Ready(),new RankingReaction(),
                new ReactionRoleListener(), new DiscordStreaming(),
                new ApplicationReaction(), new ApplicationListener());
        System.out.println("initializing listeners done");

    }

    private static void initializeCommands(){
       STATICS.COMMANDS.put("ui",new Userinfo());
       STATICS.COMMANDS.put("userinfo",new Userinfo());
       STATICS.COMMANDS.put("clear",new Clear());
       STATICS.COMMANDS.put("c",new Clear());
       STATICS.COMMANDS.put("ud",new UrbanDictonary());
       STATICS.COMMANDS.put("raffle", new Raffle());
       STATICS.COMMANDS.put("r", new Raffle());
       STATICS.COMMANDS.put("gg", new GameGami());
       STATICS.COMMANDS.put("bd", new Birthday());
       STATICS.COMMANDS.put("t", new Triggers());
       STATICS.COMMANDS.put("a", new Admin());
       STATICS.COMMANDS.put("admin", new Admin());
       STATICS.COMMANDS.put("config", new Config());
       STATICS.COMMANDS.put("naio",new Naio());
       STATICS.COMMANDS.put("help",new Naio());
       STATICS.COMMANDS.put("nick",new Owner());
       STATICS.COMMANDS.put("ticket",new Ticket());
       STATICS.COMMANDS.put("streamer",new Streamer());
       STATICS.COMMANDS.put("rr",new ReactionRole());
       STATICS.COMMANDS.put("balance",new Balance());
       STATICS.COMMANDS.put("apply",new Apply());
    }

    private static void getPremGuilds(){
        Premium.readPremGuilds();
    }

    private static void createtwitchClient(){
        STATICS.TWITCH = TwitchClientBuilder.builder()
                .withEnableHelix(true)
                //.withDefaultAuthToken(new OAuth2Credential("justfame01","ds7st8hyr7v3o3w680emd32mm3rq7r"))
                .withClientId("4gpjzppfmiy5z7o8bfp0wkc1h04lbx")
                .withClientSecret("ptstamffi68ptlipg6qre0xebdzrzo")
                .build();

        G_CONFIG.streames();


        SimpleEventHandler eventHandler = STATICS.TWITCH.getEventManager().getEventHandler(SimpleEventHandler.class);
        TwitchStreaming onLive = new TwitchStreaming(eventHandler);
        STATICS.TWITCH.getEventManager().registerEventHandler(eventHandler);

    }
}
