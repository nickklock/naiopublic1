package commands.chat;

import commands.Command;
import core.config.ConfigVals;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import static util.EMBEDS.nopowerEmbed;

public class Clear implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isCLEAR_ENABLED()) return;

        if (POWER.noPower(event.getMember())) {
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage(nopowerEmbed().build()).queue();
            return;
        }

        if (args.length < 1){
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();

            return;
        }

        if (Integer.valueOf(args[0]) <= 100){
            CMD_REACTION.positive(event);
            List<Message> history = event.getTextChannel().getHistory().retrievePast(Integer.valueOf(args[0])).complete();
            history.forEach(msg->{
                msg.delete().queue();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }else{
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).queue();
            CMD_REACTION.negative(event);
        }
    }

    @Override
    public String help(){
        return "**.clear** *Amount Msgs <= 100*";
    }

    @Override
    public String description(){
        return "Deletes Messages from the text-channel";
    }
}
