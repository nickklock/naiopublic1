package commands.sro;

import commands.Command;
import core.config.ConfigVals;
import core.config.GuildConfig;
import core.gamegamiCore.GuildInfo;
import core.gamegamiCore.HonorRanking;
import core.gamegamiCore.PlayerInfo;
import core.models.GuildInfoModel;
import core.models.UniGuild;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.*;

import java.awt.*;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.ArrayList;

public class GameGami implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isGG_ENABLED()) return;

        if (guildConfig.getGG_SERVER() == null || guildConfig.getGG_SERVER().equals("")){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure your GameGami Server\n" +
                    "use " + guildConfig.getPREFIX() + " admin change ggserver <server_name>");

            event.getTextChannel().sendMessage(embedBuilder.build()).queue();
            return;
        }

        switch (args[0]){
            case "setup":
                if (!STATICS.PREMIUM.contains(event.getGuild().getId())){
                    CMD_REACTION.negative(event);
                    return;
                }
                if (POWER.noPower(event.getMember())) return;

                if (guildConfig.getGG_RANKING_CHANNEL().isEmpty()){
                    EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure the ranking channel for gamegami\n" +
                            "use " + guildConfig.getPREFIX() + "admin change ggranking #channel");

                    event.getTextChannel().sendMessage(embedBuilder.build()).queue();

                    CMD_REACTION.negative(event);
                    return;
                }

                CMD_REACTION.positive(event);
                event.getTextChannel().sendTyping().queue();

                Message complete5 = event.getTextChannel().sendMessage(HonorRanking.getEmbed(1).build()).complete();
                //Config.setRankingMsg(complete5.getId());
                complete5.addReaction("⬅️").queue();
                complete5.addReaction("➡️").queue();
                complete5.addReaction("🔄").queue();
                guildConfig.setGG_RANKING_MSG_ID(complete5.getId());
                GuildConfig.saveConfig(guildConfig,event.getGuild().getId());
                break;
            case "pi":
            case "playerinfo":
                if (serverCheck(event, guildConfig)) return;
                event.getTextChannel().sendTyping().queue();
                try {
                    core.models.PlayerInfo info = PlayerInfo.getInfo(args[1],event.getGuild().getId());
                    EmbedBuilder infoEmbed = new EmbedBuilder().setColor(Color.decode("#3c40c6"));

                    infoEmbed.setAuthor("Playerinfo "+info.getCharName(),info.getUrl()).setThumbnail(info.getThumb())
                            .setDescription("\n\n**Set**\n"+info.getSet()+"\n\uD83C\uDF10**Latest Global**\n\n"+info.getLastestGlobal()+"\n\n☠️**Latest Unique Kill**\n\n"+info.getLatestUniqueKill())
                            .addField("\uD83D\uDC68\u200D\uD83D\uDC69\u200D\uD83D\uDC67\u200D\uD83D\uDC66 Guild",info.getGuild(),true)
                            .addField("\uD83E\uDDBA Job Alias", info.getAlias(), true)
                            .addField("\uD83C\uDD99 Level", info.getLevel().split(":")[1].trim(),true)
                            .addField("\uD83E\uDD0D HP", info.getHp(),true)
                            .addField("\uD83E\uDDE0 MP",info.getMp(),true)
                            .addField("\uD83D\uDCAA STR / INT", info.getStrInt().replace(" ","/"),true)
                            .addField("\uD83C\uDF96️ Honor Points", info.getHonor(),true)
                            .addField("\uD83D\uDCAF Points",info.getPoints().split(":")[1].trim(),true)
                            .addField("\uD83C\uDFF7️ Title",info.getTitle(),true)
                    ;
                    CMD_REACTION.positive(event);
                    event.getTextChannel().sendMessage(infoEmbed.build()).queue();
                    break;
                }catch (SocketTimeoutException | IndexOutOfBoundsException exception){
                    CMD_REACTION.negative(event);
                    MessageEmbed player_not_found = EMBEDS
                            .errorEmbed("Player not found", args[1] + " was not found.").build();
                    event.getTextChannel().sendMessage(player_not_found).queue();
                    break;

                }

            case"gi":
            case "guildinfo":
                if (serverCheck(event, guildConfig)) return;
                try {
                    GuildInfoModel gInfo = GuildInfo.getInfo(args[1], event.getGuild().getId());
                    EmbedBuilder ginfoEmbed = new EmbedBuilder().setColor(Color.decode("#575fcf"));
                    ArrayList<UniGuild> union = gInfo.getUnion();
                    StringBuilder unionGuilds = new StringBuilder();
                    union.forEach(uniGuild -> {
                        unionGuilds.append("[").append(uniGuild.getName()).append("]").append("(").append(uniGuild.getLink())
                                .append(")").append("\n");
                    });

                    ginfoEmbed.setAuthor("Guildinfo - " +gInfo.getName(),gInfo.getURL())
                            .setDescription("\n**Union**\n"+ unionGuilds.toString() +"\n")
                            .addField("\uD83E\uDDDE Leader", gInfo.getMaster(),true)
                            .addField("\uD83C\uDFC5 Rank", gInfo.getRank(),true)
                            .addField("\uD83C\uDF82 Creation Date", gInfo.getCreationDate(),true)
                            .addField("\uD83E\uDDEE Total Member", gInfo.getMemCount(),true)
                            .addField("\uD83D\uDCAF Points", gInfo.getPoints(),true)
                            .addField("\uD83C\uDD99 Level", gInfo.getLevel(),true)
                    ;

                    event.getTextChannel().sendMessage(ginfoEmbed.build()).queue();
                    break;

                }catch (SocketTimeoutException | IndexOutOfBoundsException exception){
                    CMD_REACTION.negative(event);
                    MessageEmbed player_not_found = EMBEDS
                            .errorEmbed("Guild not found", args[1] + " was not found.").build();
                    event.getTextChannel().sendMessage(player_not_found).queue();
                    break;
                }

        }
    }

    private boolean serverCheck(MessageReceivedEvent event, ConfigVals guildConfig){
        if (guildConfig.getGG_SERVER().isEmpty()){
            EmbedBuilder embedBuilder = EMBEDS.errorEmbed("Command not configured", "Please configure your gamegami server\n" +
                    "use " + guildConfig.getPREFIX() + "admin change ggserver <servername>");

            event.getTextChannel().sendMessage(embedBuilder.build()).queue();

            CMD_REACTION.negative(event);
            return true;
        }
        return false;
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
