package core.listeners;

import core.config.ConfigVals;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.G_CONFIG;
import util.STATICS;

import javax.annotation.Nonnull;

public class ReactionRoleListener extends ListenerAdapter{
    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isRR_ROLE_ENABLED()) return;
        if (!event.getMessageId().equals(guildConfig.getREACTION_ROLE_MSG_ID())) return;
        if (event.getUser().isBot()) return;

        String reactionEmoteName = event.getReactionEmote().getName().toLowerCase();
        Member m = event.getMember();
        Guild g = event.getGuild();
        if (!guildConfig.getREACTION_MAP().containsKey(reactionEmoteName))return;
        g.addRoleToMember(m, g.getRoleById(guildConfig.getREACTION_MAP().get(reactionEmoteName))).complete();
        PrivateChannel pm = event.getUser().openPrivateChannel().complete();
        pm.sendMessage("Role succesfully added").complete();
        pm.close().complete();

    }

    @Override
    public void onMessageReactionRemove(@Nonnull MessageReactionRemoveEvent event) {
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (!guildConfig.isRR_ROLE_ENABLED()) return;

        if (!event.getMessageId().equals(guildConfig.getREACTION_ROLE_MSG_ID())) return;

        String reactionEmoteName = event.getReactionEmote().getName().toLowerCase();
        Member m = event.retrieveMember().complete();

        if (!guildConfig.getREACTION_MAP().containsKey(reactionEmoteName))return;
        if (m.getUser().getId().equals(event.getJDA().getSelfUser().getId())) return;

        System.out.println(reactionEmoteName);
        String userId = event.getUserId();
        Guild g = event.getGuild();
        g.removeRoleFromMember(userId, g.getRoleById(guildConfig.getREACTION_MAP().get(reactionEmoteName))).complete();
        PrivateChannel pm = m.getUser().openPrivateChannel().complete();
        pm.sendMessage("Role successfully removed").complete();
        pm.close().complete();;
    }
}
