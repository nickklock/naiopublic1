package core.gamegamiCore;

import core.models.FortressRankingModel;
import net.dv8tion.jda.api.EmbedBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FortressPlayerRanking {

    public static EmbedBuilder getEmbed(int part, String id){
        ArrayList<FortressRankingModel> rankingsList = getFWpRanking(id);

        EmbedBuilder embedBuilder1 = new EmbedBuilder();
        embedBuilder1.setTitle("FW Player Ranking (1/2)");
        embedBuilder1.setColor(Color.decode("#3c40c6"));
        switch (part){
            case 1:
                for (int i = 0; i< 24; i++){
                    FortressRankingModel guildRank = rankingsList.get(i);
                    embedBuilder1.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "K/D: "+guildRank.getKd()+"\n"+guildRank.getKdq(),true);
                }
                return embedBuilder1;

            case 2:
                EmbedBuilder embedBuilder2 = new EmbedBuilder(embedBuilder1).setTitle("FW Player Ranking (2/2)");
                for (int i = 25; i< rankingsList.size(); i++){
                    FortressRankingModel guildRank = rankingsList.get(i);
                    embedBuilder2.addField(guildRank.getRank()+"-"+guildRank.getName(),
                            "K/D: "+guildRank.getKd()+"\n"+guildRank.getKdq(),true);
                }
                return embedBuilder2;
        }
        return null;
    }

    public static ArrayList<FortressRankingModel> getFWpRanking(String id){
        Document doc;
        try {
            doc = Jsoup.connect("https://silkroad.gamegami.com/ranking_fortress_player.php?id="+id)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            Element guildTable = doc.select("tbody").get(0);
            Elements rows = guildTable.select("tr");
            ArrayList<FortressRankingModel> ranks = new ArrayList<>();

            for (int i = 0; i < rows.size(); i++){
                Element row = rows.get(i);
                Elements cols = row.select("td");
                FortressRankingModel gRank = new FortressRankingModel(null,null,null);
                for (int a = 0; a < cols.size() ; a++){

                    switch (a){
                        case 1:
                            String colText = cols.get(a).text();
                            gRank.setRank(colText);
                            break;
                        case 3:
                            String colText1 = cols.get(a).text();
                            gRank.setName(colText1);
                            break;

                    }
                }
                if (gRank.getName() != null){
                    ranks.add(gRank);
                }
            }
            for (int c = 0; c<ranks.size();c++){
                Elements kds = doc.select("[class=td5]");
                String kdtext = kds.get(c).text();
                List<String> kd = Arrays.asList(kdtext.split("/"));
                double kdql = Double.parseDouble(kd.get(0).trim()) / Double.parseDouble(kd.get(1).trim());
                ranks.get(c).setKdq(String.valueOf(round(kdql,2)));
                ranks.get(c).setKd(kdtext);
            }
            return ranks;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
