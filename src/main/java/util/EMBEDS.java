package util;

import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;

public class EMBEDS{
    public static EmbedBuilder helpEmbed(String help, String desc){

        return new EmbedBuilder().setColor(Color.CYAN)
                .setTitle("Help")
                .setDescription(desc)
                .addField("Instructions",help,false);
    }

    public static EmbedBuilder errorEmbed(String title, String msg){
        return new EmbedBuilder()
                .setColor(Color.RED)
                .setTitle("Error - " + title)
                .setDescription(msg);
    }

    public static EmbedBuilder nopowerEmbed(){
        return new EmbedBuilder()
                .setDescription("You cant use that feature.")
                .setTitle("Power - Error")
                .setColor(Color.RED)
                ;
    }

    public static EmbedBuilder setupEmbed(){
        return new EmbedBuilder().setColor(Color.CYAN)
                .setTitle("NAIO Setup")
                .setDescription("Welcome to the NAIO setup. This will prepare the bot to use on your server.\n If you want to continue answer with [y]");
    }


    public static EmbedBuilder nickEmbed(String s){
        return new EmbedBuilder()
                .setDescription(s) ;
    }

    public static EmbedBuilder defaultMusicEmbed (){
        return new EmbedBuilder().setColor(Color.ORANGE)
                .setTitle("No Song is playing")
                .setImage("https://image.winudf.com/v2/image/Y29tLmRhbmtleXoubXVzaWNfc2NyZWVuXzIzX2wwNWZ3NXlv/screen-23.jpg?fakeurl=1&type=.jpg")
                .setFooter("Songs in queue: 0");
    }

    public static EmbedBuilder AmongEmbed(String desc){
        return new EmbedBuilder().setColor(Color.decode("#3F474E"))
                .setTitle("AmongUs")
                .setThumbnail("https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1606236732")
                .setDescription(desc);
    }
}
