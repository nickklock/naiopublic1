package core.models;

public class PlayerRank {
    private String rank;
    private String name;
    private String points;

    public PlayerRank(String rank, String name, String points) {
        this.rank = rank;
        this.name = name;
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
