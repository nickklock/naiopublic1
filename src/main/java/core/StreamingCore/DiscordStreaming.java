package core.StreamingCore;

import core.config.ConfigVals;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceStreamEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.G_CONFIG;

import javax.annotation.Nonnull;
import java.awt.*;

public class DiscordStreaming extends ListenerAdapter{

    @Override
    public void onGuildVoiceStream(@Nonnull GuildVoiceStreamEvent event){
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());

        if (!guildConfig.isSTREAMER_ENABLED()){
            return;
        }
        if (guildConfig.getSTREAM_ANNOUCMENT_CHANNEL().isEmpty()){
            return;
        }

        VoiceChannel channel = event.getVoiceState().getChannel();

        EmbedBuilder embedBuilder = new EmbedBuilder().setColor(Color.decode("#6441a5"))
                .setTitle(event.getMember().getEffectiveName() + " is now streaming on discord !")
                .setDescription("Streaming in "+channel.getName())
                .setThumbnail(event.getMember().getUser().getAvatarUrl())
                ;

        event.getGuild().getTextChannelById(guildConfig.getSTREAM_ANNOUCMENT_CHANNEL()).sendMessage(embedBuilder.build()).queue();


    }
}
