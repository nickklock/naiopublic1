package core.application;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.STATICS;

import javax.annotation.Nonnull;
import java.awt.*;

public class ApplicationReaction extends ListenerAdapter{
    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {

        if (!event.getMessageId().equals(STATICS.EIGHT_APPLYREACTION_MSG_ID)) return;
        User user = event.retrieveUser().complete();
        if (ApplicationListener.active ){
            PrivateChannel pc = user.openPrivateChannel().complete();
            pc.sendMessage(new EmbedBuilder().setColor(Color.RED)
                    .setDescription("Currently busy try again later").build()).queue();
            return;
        }
        if (STATICS.Currentapplicants.contains(user.getId())) return;
        STATICS.Currentapplicants.add(user.getId());
        EmbedBuilder answerBed = new EmbedBuilder();
        answerBed.setDescription("I'm gonna ask you some questions. You got 5 minutes to answer all the questions\n" +
                " *You agree that your answers will be send to one of EIGHT's staff Members.*\n " +
                "**Enter y to continue or n to cancel**").setColor(Color.decode("#45aaf2"));
        user = event.retrieveUser().complete();
        PrivateChannel complete = user.openPrivateChannel().complete();
        complete.sendMessage(answerBed.build()).queue();
        ApplicationListener.active = true;
        ApplicationListener.step = 0;

    }

}
