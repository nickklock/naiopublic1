package core.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ConfigVals{
    private  String PREFIX;
    private String GUILD_ID;

    private  String RAFFLE_CHANNEL ;
    private  String BD_CHANNEL ;
    private String STREAM_ANNOUCMENT_CHANNEL;
    private String REACTION_ROLE_CHANNEL;
    private String GG_RANKING_CHANNEL;

    private  String GG_SERVER ;
    private String GG_RANKING_MSG_ID;

    private String REACTION_ROLE_MSG_ID;

    private  boolean UI_ENABLED;
    private  boolean BD_ENABLED;
    private  boolean TR_ENABLED;
    private  boolean UD_ENABLED;
    private  boolean CLEAR_ENABLED;
    private  boolean RAFFLE_ENABLED;
    private  boolean GG_ENABLED;
    private  boolean STREAMER_ENABLED;
    private  boolean RR_ROLE_ENABLED;

    private List<Object> POWER_ROLES;
    private List<String> STREAMER_NAMES;
    private HashMap<String, String> REACTION_MAP;

    public ConfigVals(String PREFIX, String GUILD_ID, String RAFFLE_CHANNEL,
                      String BD_CHANNEL, String STREAM_ANNOUCMENT_CHANNEL, String REACTION_ROLE_CHANNEL,
                      String GG_RANKING_CHANNEL, String GG_SERVER, String GG_RANKING_MSG_ID, String REACTION_ROLE_MSG_ID,
                      boolean UI_ENABLED, boolean BD_ENABLED, boolean TR_ENABLED, boolean UD_ENABLED,
                      boolean CLEAR_ENABLED, boolean RAFFLE_ENABLED, boolean GG_ENABLED,
                      boolean STREAMER_ENABLED, boolean RR_ROLE_ENABLED, List<Object> POWER_ROLES,
                      List<String> STREAMER_NAMES, HashMap<String, String> REACTION_MAP){
        this.PREFIX = PREFIX;
        this.GUILD_ID = GUILD_ID;
        this.RAFFLE_CHANNEL = RAFFLE_CHANNEL;
        this.BD_CHANNEL = BD_CHANNEL;
        this.STREAM_ANNOUCMENT_CHANNEL = STREAM_ANNOUCMENT_CHANNEL;
        this.REACTION_ROLE_CHANNEL = REACTION_ROLE_CHANNEL;
        this.GG_RANKING_CHANNEL = GG_RANKING_CHANNEL;
        this.GG_SERVER = GG_SERVER;
        this.GG_RANKING_MSG_ID = GG_RANKING_MSG_ID;
        this.REACTION_ROLE_MSG_ID = REACTION_ROLE_MSG_ID;
        this.UI_ENABLED = UI_ENABLED;
        this.BD_ENABLED = BD_ENABLED;
        this.TR_ENABLED = TR_ENABLED;
        this.UD_ENABLED = UD_ENABLED;
        this.CLEAR_ENABLED = CLEAR_ENABLED;
        this.RAFFLE_ENABLED = RAFFLE_ENABLED;
        this.GG_ENABLED = GG_ENABLED;
        this.STREAMER_ENABLED = STREAMER_ENABLED;
        this.RR_ROLE_ENABLED = RR_ROLE_ENABLED;
        this.POWER_ROLES = POWER_ROLES;
        this.STREAMER_NAMES = STREAMER_NAMES;
        this.REACTION_MAP = REACTION_MAP;
    }

    public ConfigVals(){
    }

    public String getGG_RANKING_CHANNEL(){
        return GG_RANKING_CHANNEL;
    }

    public void setGG_RANKING_CHANNEL(String GG_RANKING_CHANNEL){
        this.GG_RANKING_CHANNEL = GG_RANKING_CHANNEL;
    }

    public String getREACTION_ROLE_CHANNEL(){
        return REACTION_ROLE_CHANNEL;
    }

    public void setREACTION_ROLE_CHANNEL(String REACTION_ROLE_CHANNEL){
        this.REACTION_ROLE_CHANNEL = REACTION_ROLE_CHANNEL;
    }

    public String getREACTION_ROLE_MSG_ID(){
        return REACTION_ROLE_MSG_ID;
    }

    public void setREACTION_ROLE_MSG_ID(String REACTION_ROLE_MSG_ID){
        this.REACTION_ROLE_MSG_ID = REACTION_ROLE_MSG_ID;
    }

    public HashMap<String, String> getREACTION_MAP(){
        return REACTION_MAP;
    }

    public void setREACTION_MAP(HashMap<String, String> REACTION_MAP){
        this.REACTION_MAP = REACTION_MAP;
    }

    public boolean isRR_ROLE_ENABLED(){
        return RR_ROLE_ENABLED;
    }

    public void setRR_ROLE_ENABLED(boolean RR_ROLE_ENABLED){
        this.RR_ROLE_ENABLED = RR_ROLE_ENABLED;
    }

    public boolean isSTREAMER_ENABLED(){
        return STREAMER_ENABLED;
    }

    public void setSTREAMER_ENABLED(boolean STREAMER_ENABLED){
        this.STREAMER_ENABLED = STREAMER_ENABLED;
    }

    public List<String> getSTREAMER_NAMES(){
        return STREAMER_NAMES;
    }

    public String getSTREAM_ANNOUCMENT_CHANNEL(){
        return STREAM_ANNOUCMENT_CHANNEL;
    }

    public void setSTREAM_ANNOUCMENT_CHANNEL(String STREAM_ANNOUCMENT_CHANNEL){
        this.STREAM_ANNOUCMENT_CHANNEL = STREAM_ANNOUCMENT_CHANNEL;
    }

    public void setSTREAMER_NAMES(List<String> STREAMER_NAMES){
        this.STREAMER_NAMES = STREAMER_NAMES;
    }

    public String getGUILD_ID(){
        return GUILD_ID;
    }

    public void setGUILD_ID(String GUILD_ID){
        this.GUILD_ID = GUILD_ID;
    }

    public boolean isTR_ENABLED(){
        return TR_ENABLED;
    }

    public String getGG_RANKING_MSG_ID(){
        return GG_RANKING_MSG_ID;
    }

    public void setGG_RANKING_MSG_ID(String GG_RANKING_MSG_ID){
        this.GG_RANKING_MSG_ID = GG_RANKING_MSG_ID;
    }

    public void setTR_ENABLED(boolean TR_ENABLED){
        this.TR_ENABLED = TR_ENABLED;
    }

    public boolean isBD_ENABLED(){
        return BD_ENABLED;
    }

    public void setBD_ENABLED(boolean BD_ENABLED){
        this.BD_ENABLED = BD_ENABLED;
    }

    public String getBD_CHANNEL(){
        return BD_CHANNEL;
    }

    public void setBD_CHANNEL(String BD_CHANNEL){
        this.BD_CHANNEL = BD_CHANNEL;
    }

    public List<Object> getPOWER_ROLES(){
        return POWER_ROLES;
    }

    public void setPOWER_ROLES(List<Object> POWER_ROLES){
        this.POWER_ROLES = POWER_ROLES;
    }

    public String getPREFIX(){
        return PREFIX;
    }

    public void setPREFIX(String PREFIX){
        this.PREFIX = PREFIX;
    }

    public String getRAFFLE_CHANNEL(){
        return RAFFLE_CHANNEL;
    }

    public void setRAFFLE_CHANNEL(String RAFFLE_CHANNEL){
        this.RAFFLE_CHANNEL = RAFFLE_CHANNEL;
    }

    public String getGG_SERVER(){
        return GG_SERVER;
    }

    public void setGG_SERVER(String GG_SERVER){
        this.GG_SERVER = GG_SERVER;
    }

    public boolean isUI_ENABLED(){
        return UI_ENABLED;
    }

    public void setUI_ENABLED(boolean UI_ENABLED){
        this.UI_ENABLED = UI_ENABLED;
    }

    public boolean isUD_ENABLED(){
        return UD_ENABLED;
    }

    public void setUD_ENABLED(boolean UD_ENABLED){
        this.UD_ENABLED = UD_ENABLED;
    }

    public boolean isCLEAR_ENABLED(){
        return CLEAR_ENABLED;
    }

    public void setCLEAR_ENABLED(boolean CLEAR_ENABLED){
        this.CLEAR_ENABLED = CLEAR_ENABLED;
    }

    public boolean isRAFFLE_ENABLED(){
        return RAFFLE_ENABLED;
    }

    public void setRAFFLE_ENABLED(boolean RAFFLE_ENABLED){
        this.RAFFLE_ENABLED = RAFFLE_ENABLED;
    }

    public boolean isGG_ENABLED(){
        return GG_ENABLED;
    }

    public void setGG_ENABLED(boolean GG_ENABLED){
        this.GG_ENABLED = GG_ENABLED;
    }
}
