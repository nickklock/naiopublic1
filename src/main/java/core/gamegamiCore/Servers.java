package core.gamegamiCore;

import core.config.ConfigVals;
import util.G_CONFIG;
import util.STATICS;

public class Servers{
    public static String getID (String id){
        String gg_server = G_CONFIG.getGuildConfig(id).getGG_SERVER();
        if (gg_server.toLowerCase().equals("troya")){
            return "2";
        }if (gg_server.toLowerCase().equals("efes")){
            return "1";
        }if (gg_server.toLowerCase().equals("aspendos")){
            return "3";
        }if (gg_server.toLowerCase().equals("myra")){
            return "4";
        }if (gg_server.toLowerCase().equals("assos")){
            return "5";
        }if (gg_server.toLowerCase().equals("zeugma")){
            return "6";
        }

        return null;
    }
}
