package commands.support;

import commands.Command;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.IPermissionHolder;
import net.dv8tion.jda.api.entities.PermissionOverride;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.POWER;
import util.STATICS;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public class Ticket implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        if (!event.getGuild().getId().equals("381575939542351873")) return;

        switch (args[0]){
            case "create":
            case "c":
                if (STATICS.TICKETS.contains(event.getMember().getId())){
                    CMD_REACTION.negative(event);
                    event.getTextChannel().sendMessage(EMBEDS.errorEmbed
                            ("You already have a ticket","You already created a ticket.").build()).queue();
                    return;
                }
                TextChannel createdTicketChannel = event.getGuild().getCategoryById("793638553796083753")
                       .createTextChannel("Ticket - "+event.getAuthor().getName()+
                                "#"+event.getAuthor().getDiscriminator()).complete();

                createdTicketChannel.createPermissionOverride(event.getMember()).setAllow(
                        Permission.VIEW_CHANNEL,
                        Permission.MESSAGE_WRITE,
                        Permission.MESSAGE_READ,
                        Permission.MESSAGE_HISTORY,
                        Permission.MESSAGE_EMBED_LINKS,
                        Permission.MESSAGE_ATTACH_FILES,
                        Permission.MESSAGE_ADD_REACTION,
                        Permission.MESSAGE_EXT_EMOJI)
                        .queue();

                createdTicketChannel.createPermissionOverride(event.getGuild().getPublicRole()).setDeny(
                        Permission.VIEW_CHANNEL,
                        Permission.MESSAGE_WRITE,
                        Permission.MESSAGE_READ,
                        Permission.MESSAGE_HISTORY,
                        Permission.MESSAGE_EMBED_LINKS,
                        Permission.MESSAGE_ATTACH_FILES,
                        Permission.MESSAGE_ADD_REACTION,
                        Permission.MESSAGE_EXT_EMOJI)
                        .queue();

                STATICS.TICKETS.add(event.getMember().getId());
                CMD_REACTION.positive(event);
                break;
            case "close":
                if (event.getTextChannel().getName().startsWith("Ticket - ")){
                    event.getTextChannel().delete().queue();
                    CMD_REACTION.positive(event);
                }else {
                    CMD_REACTION.negative(event);
                }
        }
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return "Creates a support ticket.";
    }
}
