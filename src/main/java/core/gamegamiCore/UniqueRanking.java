package core.gamegamiCore;

import core.models.PlayerRank;
import net.dv8tion.jda.api.EmbedBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class UniqueRanking {
//"https://silkroad.gamegami.com/character.php?shardid=2&char="
    public static EmbedBuilder getEmbed(int part, String id){
        ArrayList<PlayerRank> rankingsList = getURanking(id);

        EmbedBuilder embedBuilder1 = new EmbedBuilder();
        embedBuilder1.setTitle("Unique Ranking (1/2)");
        embedBuilder1.setColor(Color.decode("#3c40c6"));
        switch (part){
            case 1:
                for (int i = 0; i< 24; i++){
                    PlayerRank player = rankingsList.get(i);
                    embedBuilder1.addField(player.getRank()+"-"+player.getName(),
                            "Points: "+player.getPoints(),true);
                }
                return embedBuilder1;

            case 2:
                EmbedBuilder embedBuilder2 = new EmbedBuilder(embedBuilder1).setTitle("Unique Ranking (2/2)");
                for (int i = 25; i< rankingsList.size(); i++){
                    PlayerRank player = rankingsList.get(i);
                    embedBuilder2.addField(player.getRank()+"-"+player.getName(),
                            "Points: "+player.getPoints(),true);
                }
                return embedBuilder2;
        }
        return null;
    }

    public static ArrayList<PlayerRank> getURanking(String id){
        Document doc;
        try {
            doc = Jsoup.connect("https://silkroad.gamegami.com/ranking_unique.php?id="+id)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            Element guildTable = doc.select("tbody").get(0);
            Elements rows = guildTable.select("tr");
            ArrayList<PlayerRank> ranks = new ArrayList<>();

            for (int i = 0; i < rows.size(); i++){
                Element row = rows.get(i);
                Elements cols = row.select("td");
                PlayerRank player = new PlayerRank(null,null,null);
                for (int a = 0; a < cols.size() ; a++){
                    switch (a){
                        case 1:
                            player.setRank(cols.get(a).text());
                            break;
                        case 3:
                            player.setName(cols.get(a).text());
                        case 4:
                            player.setPoints(cols.get(a).text());
                            break;
                    }
                }
                if (player.getName() != null){
                    ranks.add(player);
                }
            }

            return ranks;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
