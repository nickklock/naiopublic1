package core.commandparsing;

import core.config.ConfigVals;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.STATICS;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

public class CommandParser{
    public static long parsingTime;

    public static CommandContainer parse(String rw, MessageReceivedEvent e) {
        Instant starttime = Instant.now();

        ArrayList<String> split = new ArrayList<>();

        String raw = rw;
        String pr = ".";
        String beheaded = raw.substring(STATICS.CONFIGS.get(e.getGuild().getId()).getPREFIX().length(), raw.length());
        String[] splitBeheaded = beheaded.split(" ");

        for (String s : splitBeheaded) {
            split.add(s);
        }

        String invoke = split.get(0);
        String[] args = new String[split.size()-1];
        split.subList(1, split.size()).toArray(args);
        Instant endtime = Instant.now();
        Duration timeElapsed = Duration.between(starttime, endtime);
        parsingTime = timeElapsed.toMillis();
        return new CommandContainer(raw, beheaded, splitBeheaded, invoke, args, e);
    }


}
