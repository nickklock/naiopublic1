package commands.sro;

import commands.Command;
import core.application.ApplicationListener;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.POWER;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;

public class Apply implements Command{
    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        if (POWER.noPower(event.getMember())) return;
        switch (args[0]) {
            case "status":
                EmbedBuilder eb = new EmbedBuilder().setColor(Color.YELLOW).setTitle("Application status")
                        .addField("Active",String.valueOf(ApplicationListener.active),true)
                        .addField("Step", String.valueOf(ApplicationListener.step),true)
                        .addField("Time Created", ApplicationListener.timeCreated.toString(),true);

                event.getTextChannel().sendMessage(eb.build()).queue();
                CMD_REACTION.positive(event);
                return;
            case "reset":
                ApplicationListener.active = false;
                ApplicationListener.step = 0;
                //Application.timeCreated = null;
                CMD_REACTION.positive(event);
                return;
        }
    }

    @Override
    public String help(){
        return null;
    }

    @Override
    public String description(){
        return null;
    }
}
