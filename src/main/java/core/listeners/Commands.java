package core.listeners;

import core.CustomTriggers;
import core.commandparsing.CommandContainer;
import core.commandparsing.CommandParser;
import core.commandparsing.HandleCommand;
import core.config.ConfigVals;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.G_CONFIG;
import util.STATICS;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.text.ParseException;

public class Commands extends ListenerAdapter{

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event){
        if (event.getChannelType() != ChannelType.TEXT) return;
        ConfigVals guildConfig = G_CONFIG.getGuildConfig(event.getGuild().getId());
        if (event.getAuthor().isBot()) return;
        System.out.println(event.getGuild().getId());
        if (event.getMessage().getContentRaw().startsWith(STATICS.CONFIGS.get(event.getGuild().getId()).getPREFIX())){
            try {
                HandleCommand.handleCommand(CommandParser.parse(event.getMessage().getContentRaw(),event));
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }
        }

        if (guildConfig.isTR_ENABLED() && CustomTriggers.getRespond(event) != null  ){
            event.getTextChannel().sendTyping().queue();
            event.getTextChannel().sendMessage(CustomTriggers.getRespond(event)).queue();
            return;
        }
    }
}
